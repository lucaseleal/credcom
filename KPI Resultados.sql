--PDD e DELTA PDD
select to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy'),
	sum(pdd_biz_pro_rata) pdd,
	sum(pdd_biz_pro_rata) - coalesce(lag(sum(pdd_biz_pro_rata)) over (order by to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy')),0) delta_pdd  
from financeiro_balancodiario fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
where case when fbd.renegociacoes = 0 then fbd.dias_atraso else fbd.dias_atraso_max end <= 360 
group by 1
order by 1

--PERDA REGISTRADA NO MES
select to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy'),
	sum(pdd_biz_pro_rata) filter (where case when fbd.renegociacoes = 0 then fbd.dias_atraso else fbd.dias_atraso_max end > 360) -
		coalesce(lag(sum(pdd_biz_pro_rata) filter (where case when fbd.renegociacoes = 0 then fbd.dias_atraso else fbd.dias_atraso_max end > 360)) over (order by to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy')),0) as perda_registrada
from financeiro_balancodiario fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
group by 1

--RECUPERAÇÃO POR MES
select fbd.emprestimo_id, sum(fbd.delta_dia) filter (where case when renegociacoes = 0 then dias_atraso > 360 else dias_atraso_max > 360 end) * (-1) as delta_perda
from financeiro_balancodiario fbd
group by 1
order by 1

--CARTEIRA
select w.to_date,sum(balanco_final_pro_rata)
from financeiro_balancodiario fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
where case when fbd.renegociacoes = 0 then fbd.dias_atraso <= 360 else fbd.dias_atraso_max <= 360 end
group by 1
order by 1


--CARTEIRA FIDC
select w.to_date,
	sum(balanco_final_pro_rata)
from financeiro_balancodiario fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
where case when fbd.renegociacoes = 0 then fbd.dias_atraso <= 360 else fbd.dias_atraso_max <= 360 end and fbd.portfolio_id = 7 and fbd.status = 'Vendido'
group by 1
order by 1

--RECEITA 
select to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy'),
	tac,
	sum(juros_apropriado_total_no_mes_pro_rata) filter (where dia_do_indicador = w.dia) + sum(juros_apropriado_total_no_mes_pro_rata) filter (where fbd.delta_motivo = 'PagamentoFinal') as juros_contrato,
	sum(juros_pagamento_atrasado_acumulado_mes) filter (where dia_do_indicador = w.dia) + sum(juros_pagamento_atrasado_acumulado_mes) filter (where fbd.delta_motivo = 'PagamentoFinal') as juros_atraso,
	sum(multa_pagamento_atrasado_acumulado_mes) filter (where dia_do_indicador = w.dia) + sum(multa_pagamento_atrasado_acumulado_mes) filter (where fbd.delta_motivo = 'PagamentoFinal') as multa,
	--sum(desconto_acumulado) filter (where dia_do_indicador = w.dia) - lag(sum(desconto_acumulado) filter (where dia_do_indicador = w.dia)) over (order by to_date(to_char(dia_do_indicador,'Mon/yy'),'Mon/yy')) + sum(desconto_acumulado) filter (where fbd.delta_motivo = 'PagamentoFinal') as delta_desconto,
	sum(desconto) as delta_desconto
from financeiro_balancodiario fbd
left join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
join loan_infos li on li.loan_info_id = fbd.emprestimo_id
join(select to_date(to_char(start_date,'Mon/yy'),'Mon/yy'), sum(commission) as tac
	from loan_infos li
	join loan_requests lr on lr.loan_request_id = li.loan_info_id
	where lr.status = 'ACCEPTED'
	group by 1) tac on tac.to_date = to_date(to_char(fbd.dia_do_indicador,'Mon/yy'),'Mon/yy')
where case when fbd.renegociacoes = 0 then fbd.dias_atraso else fbd.dias_atraso_max end <= 360
group by 1,2
order by 1

--over 30,60 e 90 COINCIDENTE
select w.to_date,
	sum(balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end between 31 and 360) over_30,
	sum(balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end between 61 and 360) over_60,
	sum(balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end between 91 and 360) over_90,
	sum(balanco_final_pro_rata) filter (where case fbd.renegociacoes when 0 then fbd.dias_atraso else fbd.dias_atraso_max end <= 360 ) carteira_ate_360
from financeiro_balancodiario fbd
join (select to_date(to_char(dia,'Mon/yy'),'Mon/yy'),max(dia) as dia from workdays group by 1) w on w.dia = fbd.dia_do_indicador
group by 1
order by 1

--ORIGINAÇÃO & TICKET MEDIO
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy'),count(*) as units,sum(li.total_payment) as value,avg(li.total_payment)::decimal(100,2) as ticket_medio
from loan_requests lr
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1
order by 1



-----------------------------------------------------RELATORIO DA CLARISSE-----------------------------------------------------

--RECEITA TAC
select to_char(lr.loan_date, 'YYYY-MM'), sum(li.commission) as tac
from loan_infos li, loan_requests lr
where lr.loan_request_id = li.loan_info_id and lr.status = 'ACCEPTED' and to_char(lr.loan_date, 'YYYY-MM') = '2019-01'
group by 1
order by 1

--DESCONTOS
select consolidado.emprestimo_id, sum(desconto) as desconto_mes from
(select emprestimo_id, dia_do_indicador, portfolio_id, desconto_acumulado as desconto
from financeiro_balancodiario
where dia_do_indicador = '2019-04-30'
union
select emprestimo_id, dia_do_indicador, portfolio_id, desconto_acumulado * -1 as desconto
from financeiro_balancodiario
where dia_do_indicador = '2019-03-31'
union
select emprestimo_id, dia_do_indicador, portfolio_id, desconto_acumulado as desconto
from financeiro_balancodiario
where delta_motivo = 'PagamentoFinal'
and dia_do_indicador between '2019-03-31' and '2019-04-01'
  ) as consolidado
group by emprestimo_id;

--TUDÃO
	select extract(month from dia_do_indicador) || '-2019' as mes, emprestimo_id, 
	balanco_final_pro_rata, 
	juros_a_apropriar_pro_rata,
	juros_apropriado_total_no_mes_pro_rata, 
	juros_pagamento_atrasado_acumulado_mes,
	multa_pagamento_atrasado_acumulado_mes,
	desconto_acumulado,
	pdd_biz_pro_rata,
	delta_motivo,
	portfolio_id,
	dias_atraso,
	dias_atraso_max,
	renegociacoes
	from financeiro_balancodiario
	where dia_do_indicador > '2019-01-01' and dia_do_indicador = (date_trunc('month', dia_do_indicador) + interval '1 month' - interval '1 day')::date
union
	select extract(month from dia_do_indicador) || '-2019' as mes, emprestimo_id, 
	balanco_final_pro_rata, 
	juros_a_apropriar_pro_rata,
	juros_apropriado_total_no_mes_pro_rata, 
	juros_pagamento_atrasado_acumulado_mes,
	multa_pagamento_atrasado_acumulado_mes,
	desconto_acumulado,
	pdd_biz_pro_rata,
	delta_motivo,
	portfolio_id,
	dias_atraso,
	dias_atraso_max,
	renegociacoes
	from financeiro_balancodiario
	where dia_do_indicador > '2019-01-01' and delta_motivo = 'PagamentoFinal'
	
	
select extract(month from dia_do_indicador) || '-2019' as mes, emprestimo_id, balanco_final_pro_rata, 
juros_a_apropriar_pro_rata,
juros_apropriado_total_no_mes_pro_rata, 
juros_pagamento_atrasado_acumulado_mes,
multa_pagamento_atrasado_acumulado_mes,
desconto_acumulado,
pdd_biz_pro_rata,
delta_motivo,
portfolio_id,
dias_atraso,
dias_atraso_max
from financeiro_balancodiario
where dia_do_indicador > '2019-01-01'
union
select extract(month from dia_do_indicador) || '-2019' as mes, emprestimo_id, balanco_final_pro_rata, 
juros_a_apropriar_pro_rata,
juros_apropriado_total_no_mes_pro_rata, 
juros_pagamento_atrasado_acumulado_mes,
multa_pagamento_atrasado_acumulado_mes,
desconto_acumulado,
pdd_biz_pro_rata,
delta_motivo,
portfolio_id,
dias_atraso,
dias_atraso_max
from financeiro_balancodiario
where dia_do_indicador > '2019-01-01' 
and delta_motivo = 'PagamentoFinal'
