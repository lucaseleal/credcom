--REJEICAO AUTOMATICA - mensal
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	case when da."check" = 1 or auto.directprospect_id is not null then 'Automatico' when da."check" = 0 or manual.directprospect_id is not null then 'Manual' when o.direct_prospect_id is null then 'Automatico' else 'Automatico' end tipo,
	count(*) as units
from production_admin_public.direct_prospects dp
left join (select distinct direct_prospect_id,max("check") as "check" from sandbox.decisao_automatica group by 1) da on da.direct_prospect_id = dp.direct_prospect_id
left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome in ('Manual','Canal Manual','Renovação')) as manual on manual.directprospect_id = dp.direct_prospect_id
left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome in ('Automático','Credita')) as auto on auto.directprospect_id = dp.direct_prospect_id
left join (select distinct direct_prospect_id from production_admin_public.offers) o on o.direct_prospect_id = dp.direct_prospect_id
group by 1,2
order by 1,2

--MOTIVO PARA DERIVAÇÃO MANUAL - mensal
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	case when canal.directprospect_id is not null then lead_marketing_channel when renew.directprospect_id is not null then 'Renovação' else 'Outro' end as tipo,
	count(*) as units
from production_admin_public.direct_prospects dp
join (select direct_prospect_id,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	from production_admin_public.direct_prospects) as t1 on t1.direct_prospect_id = dp.direct_prospect_id
left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome = 'Canal Manual') as canal on canal.directprospect_id = dp.direct_prospect_id
left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome = 'Renovação') as renew on renew.directprospect_id = dp.direct_prospect_id
join(select dp.direct_prospect_id,case when da."check" = 1 or auto.directprospect_id is not null then 'Automatico' when da."check" = 0 or manual.directprospect_id is not null then 'Manual' when o.direct_prospect_id is null then 'Automatico' else 'Automatico' end as tipo
	from production_admin_public.direct_prospects dp
	left join (select distinct direct_prospect_id,max("check") as "check" from sandbox.decisao_automatica group by 1) da on da.direct_prospect_id = dp.direct_prospect_id
	left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome in ('Manual','Canal Manual','Renovação')) as manual on manual.directprospect_id = dp.direct_prospect_id
	left join (select distinct dpt.directprospect_id from production_admin_public.direct_prospects_tag dpt join production_admin_public.tags t on t.id = dpt.tag_id where t.nome in ('Automático','Credita')) as auto on auto.directprospect_id = dp.direct_prospect_id
	left join (select distinct direct_prospect_id from production_admin_public.offers) o on o.direct_prospect_id = dp.direct_prospect_id) as t2 on t2.direct_prospect_id = dp.direct_prospect_id and t2.tipo = 'Manual'
group by 1,2
order by 1,2
