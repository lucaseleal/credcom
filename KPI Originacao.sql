--Evolutivo propostas
select to_date(to_char(opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*),
	sum(amount_requested)
from direct_prospects
group by 1



--estado
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null or dp.state = '') as "Sem estado"
--------------------------
from direct_prospects dp
group by 1


--faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
group by 1

select count(*) from direct_prospects

--idade
select to_date(to_char(opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
--------------------------
from direct_prospects 
group by 1

--Evolutivo propostas setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
group by 1


--Evolutivo propostas canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	from direct_prospects dp) as t1
group by 1

select dp.direct_prospect_id
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
where dp."name" like '%CALOS AL'

--Evolutivo propostas compra recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
--------------------------TABLE CLIENTS
left join (select dp.direct_prospect_id, t1.is_renew
	from direct_prospects dp
	join (select dp.cnpj, dp.opt_in_date, count(*) filter (where dp.opt_in_date > t1.request_date) as is_renew
		from direct_prospects dp
		join clients c on c.cnpj = dp.cnpj
		join (select o.client_id, lr.loan_request_id, lr.loan_date as request_date
			from offers o
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = c.client_id
		group by 1,2) as t1 on t1.cnpj = dp.cnpj and t1.opt_in_date = dp.opt_in_date) as isrenew on isrenew.direct_prospect_id = dp.direct_prospect_id
group by 1


--Evolutivo propostas motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
group by 1


--Qualificado
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) as "Unidades",
	SUM(dp.amount_requested) as "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or.direct_prospect_id is not null
group by 1

--Bizu
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu_score >= 850) as "A+",
	count(*) filter (where bizu_score between 700 and 849.99) as "A-",
	count(*) filter (where bizu_score between 650 and 699.99) as "B+",
	count(*) filter (where bizu_score between 600 and 649.99) as "B-",
	count(*) filter (where bizu_score between 500 and 599.99) as "C+",
	count(*) filter (where bizu_score between 400 and 499.99) as "C-",
	count(*) filter (where bizu_score between 300 and 399.99) as "D",	
	count(*) filter (where bizu_score < 300) as "E",
	count(*) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or.direct_prospect_id is not null
group by 1

--Lead Score
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lead_score >= 800) as "A+",
	count(*) filter (where lead_score between 700 and 799.99) as "A-",
	count(*) filter (where lead_score between 600 and 699.99) as "B+",
	count(*) filter (where lead_score between 500 and 599.99) as "B-",
	count(*) filter (where lead_score between 400 and 499.99) as "C+",
	count(*) filter (where lead_score between 300 and 399.99) as "C-",
	count(*) filter (where lead_score between 200 and 299.99) as "D",	
	count(*) filter (where lead_score < 200) as "E",
	count(*) filter (where bizu_score is null) as "Sem lead"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) as "A+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) as "A-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) as "B+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) as "B-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) as "C+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) as "C-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) as "D",	
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) as "E",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1

--Bizu 2 Score
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu2_score >= 1100) as "A+",
	count(*) filter (where bizu2_score between 850 and 1099.99) as "A-",
	count(*) filter (where bizu2_score between 720 and 849.99) as "B+",
	count(*) filter (where bizu2_score between 600 and 719.99) as "B-",
	count(*) filter (where bizu2_score between 500 and 599.99) as "C+",
	count(*) filter (where bizu2_score between 400 and 499.99) as "C-",
	count(*) filter (where bizu2_score between 300 and 399.99) as "D",	
	count(*) filter (where bizu2_score between 200 and 299.99) as "E",	
	count(*) filter (where bizu2_score < 200) as "F",
	count(*) filter (where bizu2_score is null) as "Sem bizu 2"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >= 300) or o.direct_prospect_id is not null 
group by 1

--Media Scores
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(coalesce(serasa_coleta,o.rating,dp.serasa_4)) as "Avg Serasa",
	avg(dp.lead_score) as "Avg Lead Score",
	avg(dp.bizu2_score) as "Avg Bizu 2"	
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null or dp.state = '') as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1



--Idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null) as t1
group by 1


--Compra recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE CLIENTS
left join (select dp.direct_prospect_id, t1.is_renew
	from direct_prospects dp
	join (select dp.cnpj, dp.opt_in_date, count(*) filter (where dp.opt_in_date > t1.request_date) as is_renew
		from direct_prospects dp
		join clients c on c.cnpj = dp.cnpj
		join (select o.client_id, lr.loan_request_id, lr.loan_date as request_date
			from offers o
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = c.client_id
		group by 1,2) as t1 on t1.cnpj = dp.cnpj and t1.opt_in_date = dp.opt_in_date) as isrenew on isrenew.direct_prospect_id = dp.direct_prospect_id
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--Motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null
group by 1


--OFERTA
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) "Unidades",
	SUM(o.max_value) "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1

--Evolutivo ofertas bizu
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu_score >= 850) as "A+",
	count(*) filter (where bizu_score between 700 and 849.99) as "A-",
	count(*) filter (where bizu_score between 650 and 699.99) as "B+",
	count(*) filter (where bizu_score between 600 and 649.99) as "B-",
	count(*) filter (where bizu_score between 500 and 599.99) as "C+",
	count(*) filter (where bizu_score between 400 and 499.99) as "C-",
	count(*) filter (where bizu_score between 300 and 399.99) as "D",	
	count(*) filter (where bizu_score < 300) as "E",
	count(*) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1


--Evolutivo ofertas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) as "A+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) as "A-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) as "B+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) as "B-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) as "C+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) as "C-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) as "D",	
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) as "E",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where o.direct_prospect_id is not null
group by 1


--Evolutivo ofertas regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null and dp.state = '') as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1


--Evolutivo ofertas faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1



--Evolutivo ofertas idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1

--Evolutivo ofertas setor
select --to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
--group by 1


--Evolutivo oferta canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	where o.direct_prospect_id is not null) as t1
group by 1


--Evolutivo ofertas compra recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE CLIENTS
left join (select o.offer_id, t1.is_renew
	from offers o 
	join (select o.client_id, o.date_inserted, count(*) filter (where o.date_inserted > t1.request_date) as is_renew
		from offers o
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = o.date_inserted) as isrenew on isrenew.offer_id = o.offer_id
where o.direct_prospect_id is not null
group by 1

--motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null
group by 1




--Pedidos
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) "Unidades",
	SUM(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end) as "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1

--Bizu
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu_score >= 850) as "A+",
	count(*) filter (where bizu_score between 700 and 849.99) as "A-",
	count(*) filter (where bizu_score between 650 and 699.99) as "B+",
	count(*) filter (where bizu_score between 600 and 649.99) as "B-",
	count(*) filter (where bizu_score between 500 and 599.99) as "C+",
	count(*) filter (where bizu_score between 400 and 499.99) as "C-",
	count(*) filter (where bizu_score between 300 and 399.99) as "D",	
	count(*) filter (where bizu_score < 300) as "E",
	count(*) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1


--Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) as "A+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) as "A-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) as "B+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) as "B-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) as "C+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) as "C-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) as "D",	
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) as "E",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.loan_request_id is not null
group by 1


--Regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null or dp.state = '') as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1


--Faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1


--Idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
---------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1


--Setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1


--Canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	where lr.loan_request_id is not null) as t1
group by 1



--Recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE CLIENTS
left join (select lr.loan_request_id, t1.is_renew
	from offers o 
	join loan_requests lr on lr.offer_id = o.offer_id 
	join (select o.client_id, lr.date_inserted, count(*) filter (where lr.date_inserted > t1.request_date) as is_renew
		from offers o
		join loan_requests lr on lr.offer_id = o.offer_id
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = lr.date_inserted) as isrenew on isrenew.loan_request_id = lr.loan_request_id
where lr.loan_request_id is not null
group by 1


--motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where lr.loan_request_id is not null
group by 1



--Pedidos completos
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) "Unidades",
	SUM(coalesce(lr.valor_solicitado,lr.value)) as "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)
group by 1

--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),lr.status,
	count(*) "Unidades"
--	SUM(coalesce(lr.valor_solicitado,lr.value)) as "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) and dp.opt_in_date >= '2018-10-01'
group by 1,2



--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),o.razao_perda,
	count(*) "Unidades",avg(lr.valor_solicitado)
--	SUM(coalesce(lr.valor_solicitado,lr.value)) as "Valor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where not (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) and dp.opt_in_date >= '2018-10-01' and lr.loan_request_id is not null and lr.status = 'Expirado'
group by 1,2







--Bizu
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu_score >= 850) as "A+",
	count(*) filter (where bizu_score between 700 and 849.99) as "A-",
	count(*) filter (where bizu_score between 650 and 699.99) as "B+",
	count(*) filter (where bizu_score between 600 and 649.99) as "B-",
	count(*) filter (where bizu_score between 500 and 599.99) as "C+",
	count(*) filter (where bizu_score between 400 and 499.99) as "C-",
	count(*) filter (where bizu_score between 300 and 399.99) as "D",	
	count(*) filter (where bizu_score < 300) as "E",
	count(*) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) as "A+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) as "A-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) as "B+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) as "B-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) as "C+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) as "C-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) as "D",	
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) as "E",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1



--Idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
	left join alldocs ad on ad.loan_request_id = lr.loan_request_id
	where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) as t1
group by 1

--Recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select lr.loan_request_id, t1.is_renew
	from offers o 
	join loan_requests lr on lr.offer_id = o.offer_id 
	join (select o.client_id, lr.date_inserted, count(*) filter (where lr.date_inserted > t1.request_date) as is_renew
		from offers o
		join loan_requests lr on lr.offer_id = o.offer_id
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = lr.date_inserted) as isrenew on isrenew.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null
group by 1


--Loans
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) as "Unidades",
	SUM(li.total_payment) as "Valor"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo
select 
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy') = '2017') as "2017",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy') = '2018') as "2018",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')) as "Q1_2018",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')) as "Q2_2018",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')) as "Q3_2018",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')) as "Q4_2018",
	sum(li.total_payment) filter (where to_char(lr.loan_date,'yyyy-MM') in ('2019-01','2019-02','2019-03')) as "Q1_2019"
--------------------------
from direct_prospects dp
join offers o on dp.direct_prospect_id = o.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'

--Bizu
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu_score >= 850)::float as "A+",
	count(*) filter (where bizu_score between 700 and 849.99)::float as "A-", 
	count(*) filter (where bizu_score between 650 and 699.99)::float as "B+",
	count(*) filter (where bizu_score between 600 and 649.99)::float as "B-",
	count(*) filter (where bizu_score between 500 and 599.99)::float as "C+",
	count(*) filter (where bizu_score between 400 and 499.99)::float as "C-",
	count(*) filter (where bizu_score between 300 and 399.99)::float as "D",
	count(*) filter (where bizu_score < 300)::float as "E",
	count(*) filter (where bizu_score is null)::float as "Sem bizu"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Bizu 2
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu2_score >= 1100)::float as "A+",
	count(*) filter (where bizu2_score between 850 and 1099.99)::float as "A-", 
	count(*) filter (where bizu2_score between 720 and 849.99)::float as "B+",
	count(*) filter (where bizu2_score between 600 and 719.99)::float as "B-",
	count(*) filter (where bizu2_score between 500 and 599.99)::float as "C+",
	count(*) filter (where bizu2_score between 400 and 499.99)::float as "C-",
	count(*) filter (where bizu2_score between 300 and 399.99)::float as "D",
	count(*) filter (where bizu2_score between 200 and 299.99)::float as "E",
	count(*) filter (where bizu2_score < 200)::float as "F",
	count(*) filter (where bizu2_score is null)::float as "Sem bizu2"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
where lr.status = 'ACCEPTED'
group by 1



--Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) as "A+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) as "A-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) as "B+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) as "B-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) as "C+",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) as "C-",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) as "D",	
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) as "E",
	count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.state = 'SP') as "SAO PAULO",
	count(*) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	count(*) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	count(*) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	count(*) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.month_revenue > 400000) as ">400k",
	count(*) filter (where dp.month_revenue between 200000 and 400000) as ">200k",
	count(*) filter (where dp.month_revenue between 80000 and 199999) as ">80k",
	count(*) filter (where dp.month_revenue between 40000 and 79999) as ">40k",
	count(*) filter (where dp.month_revenue between 10000 and 39999) as ">10k",
	count(*) filter (where dp.month_revenue < 10000) as "<10k",
	count(*) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where age > 20) as ">20",
	count(*) filter (where age >=10 and age <= 20) as ">10",
	count(*) filter (where age >=5 and age < 10) as ">5",
	count(*) filter (where age >=3 and age < 5) as ">3",
	count(*) filter (where age >=2 and age < 3) as ">2",
	count(*) filter (where age >=1 and age < 2) as ">1",
	count(*) filter (where age < 1) as "<1",
	count(*) filter (where age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.sector = 'COMERCIO') as "Comércio",
	count(*) filter (where dp.sector = 'SERVICOS') as "Serviços",
	count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	count(*) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	count(*) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	count(*) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1




--Canal
select to_date,
	count(*) filter (where lead_marketing_channel = 'facebook') "facebook",
	count(*) filter (where lead_marketing_channel = 'organico') "organico",
	count(*) filter (where lead_marketing_channel = 'adwords') "google",
	count(*) filter (where lead_marketing_channel = 'android') as "mobile",
	count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) as "grande parceiros",
	count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) "pequenos parceiros",
	count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) "outras midias",
	count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) "outros canais",
	count(*) filter (where lead_marketing_channel = 'NULL') "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1



--Recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where is_renew = 0 or is_renew is null) as "Compra",
	count(*) filter (where is_renew > 0) as "Recompra"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE CLIENTS
left join (select lr.loan_request_id, t1.is_renew
	from offers o 
	join loan_requests lr on lr.offer_id = o.offer_id 
	join (select o.client_id, lr.date_inserted, count(*) filter (where lr.date_inserted > t1.request_date) as is_renew
		from offers o
		join loan_requests lr on lr.offer_id = o.offer_id
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = o.client_id
		group by 1,2) as t1 on t1.client_id = o.client_id and t1.date_inserted = lr.date_inserted) as isrenew on isrenew.loan_request_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.reason_for_loan = 'Capital de Giro') as "Capital de Giro",
	count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') as "Compra de Estoque",
	count(*) filter (where dp.reason_for_loan = 'Expansão') as "Expansão",
	count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') as "Consolidação de Dívidas",
	count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') as "Compra de Equipamentos",
	count(*) filter (where dp.reason_for_loan = 'Reforma') as "Reforma",
	count(*) filter (where dp.reason_for_loan = 'Outros') as "Outros",
	count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') as "Marketing e Vendas",
	count(*) filter (where dp.reason_for_loan = 'Pessoal') as "Pessoal",
	count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') as "Uso Pessoal",
	count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) as "Sem motivo"
--------------------------
from direct_prospects dp
join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1





--taxa aprovação
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------


--Evolutivo (units)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*)filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) as "Total approved leads",
	count(*)filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido'))::float / count (*)::float as "Approval rate (over total leads)",
	count(*)filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido'))::float / count (*) filter (where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa'))::float as "Approval rate (over target population leads)"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
group by 1

--Evolutivo (value)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	sum(dp.amount_requested)filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) as "Total approved leads",
	sum(o.max_value)filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido'))::float / sum (dp.amount_requested)::float as "Approval rate (over total leads)",
	sum(coalesce(lr.value,o.max_value)) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido'))::float / sum (dp.amount_requested) filter (where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa'))::float as "Approval rate (over target population leads)"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
group by 1


--Bizu 2
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu2_score >= 1100 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score >= 1100) when 0 then 1 else count(*) filter (where dp.bizu2_score >= 1100) end::float as "A+",
	count(*) filter (where dp.bizu2_score between 850 and 1099.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 850 and 1099.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 850 and 1099.99) end::float as "A-",
	count(*) filter (where dp.bizu2_score between 720 and 849.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 720 and 849.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 720 and 849.99) end::float as "B+",
	count(*) filter (where dp.bizu2_score between 600 and 719.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 600 and 719.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 600 and 719.99) end::float as "B-",
	count(*) filter (where dp.bizu2_score between 500 and 599.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 500 and 599.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 500 and 599.99) end::float as "C+",
	count(*) filter (where dp.bizu2_score between 400 and 499.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 400 and 499.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 400 and 499.99) end::float as "C-",
	count(*) filter (where dp.bizu2_score between 300 and 399.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 300 and 399.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 300 and 399.99) end::float as "D",
	count(*) filter (where dp.bizu2_score between 200 and 299.99 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score between 200 and 299.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 200 and 299.99) end::float as "E",
	count(*) filter (where dp.bizu2_score < 200 and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score < 200) when 0 then 1 else count(*) filter (where dp.bizu2_score < 200) end::float as "F",
	count(*) filter (where dp.bizu2_score is null and dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')) / case count(*) filter (where dp.bizu2_score is null) when 0 then 1 else count(*) filter (where dp.bizu2_score is null) end::float as "Sem bizu 2"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1


--Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) end::float as "A+",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) end::float as "A-",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) end::float as "B+",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) end::float as "B-",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) end::float as "C+",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) end::float as "C-",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) end::float as "D",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) end::float as "E",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and  coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) end::float as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Regiao
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state = 'SP') / case count(*) filter (where dp.state = 'SP') when 0 then 1 else count(*) filter (where dp.state = 'SP') end::float as "SAO PAULO",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state in ('RJ','MG','ES')) / case count(*) filter (where dp.state in ('RJ','MG','ES')) when 0 then 1 else count(*) filter (where dp.state in ('RJ','MG','ES')) end::float as "SUDESTE",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state in ('PR','RS','SC')) / case count(*) filter (where dp.state in ('PR','RS','SC')) when 0 then 1 else count(*) filter (where dp.state in ('PR','RS','SC')) end::float as "SUL",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) / case count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) when 0 then 1 else count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) end::float as "NORDESTE",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state in ('MT','MS','GO','DF')) / case count(*) filter (where dp.state in ('MT','MS','GO','DF')) when 0 then 1 else count(*) filter (where dp.state in ('MT','MS','GO','DF')) end::float as "CENTRO OESTE",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state in ('AM','RR','AP','PA','TO','RO','AC')) / case count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) when 0 then 1 else count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) end::float as "NORTE",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.state is null) / case count(*) filter (where dp.state is null) when 0 then 1 else count(*) filter (where dp.state is null) end::float as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1



--Faturamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue > 400000) / case count(*) filter (where dp.month_revenue > 400000) when 0 then 1 else count(*) filter (where dp.month_revenue > 400000) end::float as ">400k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue between 200000 and 400000) / case count(*) filter (where dp.month_revenue between 200000 and 400000) when 0 then 1 else count(*) filter (where dp.month_revenue between 200000 and 400000) end::float as ">200k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue between 80000 and 199999) / case count(*) filter (where dp.month_revenue between 80000 and 199999) when 0 then 1 else count(*) filter (where dp.month_revenue between 80000 and 199999) end::float as ">80k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue between 40000 and 79999) / case count(*) filter (where dp.month_revenue between 40000 and 79999) when 0 then 1 else count(*) filter (where dp.month_revenue between 40000 and 79999) end::float as ">40k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue between 10000 and 39999) / case count(*) filter (where dp.month_revenue between 10000 and 39999) when 0 then 1 else count(*) filter (where dp.month_revenue between 10000 and 39999) end::float as ">10k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue < 10000) / case count(*) filter (where dp.month_revenue < 10000) when 0 then 1 else count(*) filter (where dp.month_revenue < 10000) end::float as "<10k",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.month_revenue is null) / case count(*) filter (where dp.month_revenue is null) when 0 then 1 else count(*) filter (where dp.month_revenue is null) end::float as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Idade
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.age > 20) / case count(*) filter (where age > 20) when 0 then 1 else count(*) filter (where age > 20) end::float as ">20",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and age >=10 and age <= 20) / case count(*) filter (where age >=10 and age <= 20) when 0 then 1 else count(*) filter (where age >=10 and age <= 20) end::float as ">10",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and age >=5 and age < 10) / case count(*) filter (where age >=5 and age < 10) when 0 then 1 else count(*) filter (where age >=5 and age < 10) end::float as ">5",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and age >=3 and age < 5) / case count(*) filter (where age >=3 and age < 5) when 0 then 1 else count(*) filter (where age >=3 and age < 5) end::float as ">3",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and age >=2 and age < 3) / case count(*) filter (where age >=2 and age < 3) when 0 then 1 else count(*) filter (where age >=2 and age < 3) end::float as ">2",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and age >=1 and age < 2) / case count(*) filter (where age >=1 and age < 2) when 0 then 1 else count(*) filter (where age >=1 and age < 2) end::float as ">1",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.age < 1) / case count(*) filter (where age < 1) when 0 then 1 else count(*) filter (where age < 1) end::float as "<1",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.age is null) / case count(*) filter (where age is null) when 0 then 1 else count(*) filter (where age is null) end::float as "Sem idade"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Setor
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = 'COMERCIO') / case count(*) filter (where dp.sector = 'COMERCIO') when 0 then 1 else count(*) filter (where dp.sector = 'COMERCIO') end::float as "Comércio",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = 'SERVICOS') / case count(*) filter (where dp.sector = 'SERVICOS') when 0 then 1 else count(*) filter (where dp.sector = 'SERVICOS') end::float as "Serviços",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = 'CONSTRUCAO CIVIL') / case count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') when 0 then 1 else count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') end::float as "Construção civil",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = 'AGROPECUARIA') / case count(*) filter (where dp.sector = 'AGROPECUARIA') when 0 then 1 else count(*) filter (where dp.sector = 'AGROPECUARIA') end::float as "Agropecuaria",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = 'INDUSTRIA') / case count(*) filter (where dp.sector = 'INDUSTRIA') when 0 then 1 else count(*) filter (where dp.sector = 'INDUSTRIA') end::float as "Industria",
	count(*) filter (where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.sector = '' or dp.sector is null) / case count(*) filter (where dp.sector = '' or dp.sector is null) when 0 then 1 else count(*) filter (where dp.sector = '' or dp.sector is null) end::float as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1




--Canal
select to_date,
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel = 'facebook') / case count(*) filter (where lead_marketing_channel = 'facebook') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'facebook') end::float as "facebook",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel = 'organico') / case count(*) filter (where lead_marketing_channel = 'organico') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'organico') end::float as "organico",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel = 'adwords') / case count(*) filter (where lead_marketing_channel = 'adwords') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'adwords') end::float as "google",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel = 'android') / case count(*) filter (where lead_marketing_channel = 'android') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'android') end::float as "mobile",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) / case count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) end::float as "grande parceiros",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) / case count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) end::float as "pequenos parceiros",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel in ('taboola','linkedin','twitter','bing')) / case count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) end::float as "outras midias",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) / case count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) end::float as "outros canais",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(status_oferta,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(status_pedido,'Sem pedido') not in ('Cancelado','Substituido') and lead_marketing_channel = 'NULL') / case count(*) filter (where lead_marketing_channel = 'NULL') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'NULL') end::float as "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),dp.workflow, o.status as status_oferta,lr.status as status_pedido,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end) as t1
where workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--compra recompra
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and (is_renew = 0 or is_renew is null)) / case count(*) filter (where is_renew = 0 or is_renew is null) when 0 then 1 else count(*) filter (where is_renew = 0 or is_renew is null) end::float as "Compra",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and is_renew > 0) / case count(*) filter (where is_renew > 0) when 0 then 1 else count(*) filter (where is_renew > 0) end::float as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE CLIENTS
left join (select dp.direct_prospect_id, t1.is_renew
	from direct_prospects dp
	join (select dp.client_id, opt_in_date, count(*) filter (where opt_in_date > t1.request_date) as is_renew
		from direct_prospects dp
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = dp.client_id
		group by 1,2) as t1 on t1.client_id = dp.client_id and t1.opt_in_date = dp.opt_in_date) as isrenew on isrenew.direct_prospect_id = dp.direct_prospect_id
group by 1

--motivo
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Capital de Giro') / case count(*) filter (where dp.reason_for_loan = 'Capital de Giro') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Capital de Giro') end::float as "Capital de Giro",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Compra de Estoque') / case count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') end::float as "Compra de Estoque",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Expansão') / case count(*) filter (where dp.reason_for_loan = 'Expansão') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Expansão') end::float as "Expansão",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Consolidação de Dívidas') / case count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') end::float as "Consolidação de Dívidas",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Compra de Equipamentos') / case count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') end::float as "Compra de Equipamentos",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Reforma') / case count(*) filter (where dp.reason_for_loan = 'Reforma') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Reforma') end::float as "Reforma",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Outros') / case count(*) filter (where dp.reason_for_loan = 'Outros') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Outros') end::float as "Outros",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Marketing e Vendas') / case count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') end::float as "Marketing e Vendas",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Pessoal') / case count(*) filter (where dp.reason_for_loan = 'Pessoal') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Pessoal') end::float as "Pessoal",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Uso Pessoal') / case count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') end::float as "Uso Pessoal",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and (dp.reason_for_loan = '' or dp.reason_for_loan is null)) / case count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) when 0 then 1 else count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) end::float as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1


--taxa de efetivação
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------


--Evolutivo (units)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*)filter (where lr.status = 'ACCEPTED') as "Total approved leads",
	count(*)filter (where lr.status = 'ACCEPTED')::float / count (*)::float as "Approval rate (over total leads)",
	count(*)filter (where lr.status = 'ACCEPTED')::float / count (*) filter (where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa'))::float as "Approval rate (over target population leads)"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
group by 1

--Evolutivo (value)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	sum(dp.amount_requested)filter (where lr.status = 'ACCEPTED') as "Total approved leads",
	sum(o.max_value)filter (where lr.status = 'ACCEPTED')::float / sum (dp.amount_requested)::float as "Approval rate (over total leads)",
	sum(coalesce(lr.value,o.max_value)) filter (where lr.status = 'ACCEPTED')::float / sum (dp.amount_requested) filter (where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa'))::float as "Approval rate (over target population leads)"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
group by 1


--Bizu 2 (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where bizu2_score >= 1100 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score >= 1100) when 0 then 1 else count(*) filter (where dp.bizu2_score >= 1100) end::float as "A+",
	count(*) filter (where dp.bizu2_score between 850 and 1099.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 850 and 1099.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 850 and 1099.99) end::float as "A-",
	count(*) filter (where dp.bizu2_score between 720 and 849.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 720 and 849.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 720 and 849.99) end::float as "B+",
	count(*) filter (where dp.bizu2_score between 600 and 719.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 600 and 719.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 600 and 719.99) end::float as "B-",
	count(*) filter (where dp.bizu2_score between 500 and 599.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 500 and 599.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 500 and 599.99) end::float as "C+",
	count(*) filter (where dp.bizu2_score between 400 and 499.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 400 and 499.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 400 and 499.99) end::float as "C-",
	count(*) filter (where dp.bizu2_score between 300 and 399.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 300 and 399.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 300 and 399.99) end::float as "D",
	count(*) filter (where dp.bizu2_score between 200 and 299.99 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score between 200 and 299.99) when 0 then 1 else count(*) filter (where dp.bizu2_score between 200 and 299.99) end::float as "E",
	count(*) filter (where dp.bizu2_score < 200 and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score < 200) when 0 then 1 else count(*) filter (where dp.bizu2_score < 200) end::float as "F",
	count(*) filter (where dp.bizu2_score is null and lr.status = 'ACCEPTED') / case count(*) filter (where dp.bizu2_score is null) when 0 then 1 else count(*) filter (where dp.bizu2_score is null) end::float as "Sem bizu 2"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1


--Serasa (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) >= 750) end::float as "A+",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 600 and 749.99) end::float as "A-",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 450 and 599.99) end::float as "B+",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 350 and 449.99) end::float as "B-",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 250 and 349.99) end::float as "C+",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 150 and 249.99) end::float as "C-",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) between 100 and 149.99) end::float as "D",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) < 100) end::float as "E",
	count(*) filter (where lr.status = 'ACCEPTED' and coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) / case count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) when 0 then 1 else count(*) filter (where coalesce(serasa_coleta,o.rating,dp.serasa_4) is null) end::float as "Sem serasa"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo in ('Serasa','RelatoBasico','RelatoSocios')
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Regiao (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state = 'SP') / case count(*) filter (where dp.state = 'SP') when 0 then 1 else count(*) filter (where dp.state = 'SP') end::float as "SAO PAULO",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state in ('RJ','MG','ES')) / case count(*) filter (where dp.state in ('RJ','MG','ES')) when 0 then 1 else count(*) filter (where dp.state in ('RJ','MG','ES')) end::float as "SUDESTE",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state in ('PR','RS','SC')) / case count(*) filter (where dp.state in ('PR','RS','SC')) when 0 then 1 else count(*) filter (where dp.state in ('PR','RS','SC')) end::float as "SUL",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) / case count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) when 0 then 1 else count(*) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) end::float as "NORDESTE",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state in ('MT','MS','GO','DF')) / case count(*) filter (where dp.state in ('MT','MS','GO','DF')) when 0 then 1 else count(*) filter (where dp.state in ('MT','MS','GO','DF')) end::float as "CENTRO OESTE",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state in ('AM','RR','AP','PA','TO','RO','AC')) / case count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) when 0 then 1 else count(*) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) end::float as "NORTE",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.state is null) / case count(*) filter (where dp.state is null) when 0 then 1 else count(*) filter (where dp.state is null) end::float as "Sem estado"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Faturamento (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue > 400000) / case count(*) filter (where dp.month_revenue > 400000) when 0 then 1 else count(*) filter (where dp.month_revenue > 400000) end::float as ">400k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue between 200000 and 400000) / case count(*) filter (where dp.month_revenue between 200000 and 400000) when 0 then 1 else count(*) filter (where dp.month_revenue between 200000 and 400000) end::float as ">200k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue between 80000 and 199999) / case count(*) filter (where dp.month_revenue between 80000 and 199999) when 0 then 1 else count(*) filter (where dp.month_revenue between 80000 and 199999) end::float as ">80k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue between 40000 and 79999) / case count(*) filter (where dp.month_revenue between 40000 and 79999) when 0 then 1 else count(*) filter (where dp.month_revenue between 40000 and 79999) end::float as ">40k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue between 10000 and 39999) / case count(*) filter (where dp.month_revenue between 10000 and 39999) when 0 then 1 else count(*) filter (where dp.month_revenue between 10000 and 39999) end::float as ">10k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue < 10000) / case count(*) filter (where dp.month_revenue < 10000) when 0 then 1 else count(*) filter (where dp.month_revenue < 10000) end::float as "<10k",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.month_revenue is null) / case count(*) filter (where dp.month_revenue is null) when 0 then 1 else count(*) filter (where dp.month_revenue is null) end::float as "Sem faturamento"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Idade (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lr.status = 'ACCEPTED' and dp.age > 20) / case count(*) filter (where age > 20) when 0 then 1 else count(*) filter (where age > 20) end::float as ">20",
	count(*) filter (where lr.status = 'ACCEPTED' and age >=10 and age <= 20) / case count(*) filter (where age >=10 and age <= 20) when 0 then 1 else count(*) filter (where age >=10 and age <= 20) end::float as ">10",
	count(*) filter (where lr.status = 'ACCEPTED' and age >=5 and age < 10) / case count(*) filter (where age >=5 and age < 10) when 0 then 1 else count(*) filter (where age >=5 and age < 10) end::float as ">5",
	count(*) filter (where lr.status = 'ACCEPTED' and age >=3 and age < 5) / case count(*) filter (where age >=3 and age < 5) when 0 then 1 else count(*) filter (where age >=3 and age < 5) end::float as ">3",
	count(*) filter (where lr.status = 'ACCEPTED' and age >=2 and age < 3) / case count(*) filter (where age >=2 and age < 3) when 0 then 1 else count(*) filter (where age >=2 and age < 3) end::float as ">2",
	count(*) filter (where lr.status = 'ACCEPTED' and age >=1 and age < 2) / case count(*) filter (where age >=1 and age < 2) when 0 then 1 else count(*) filter (where age >=1 and age < 2) end::float as ">1",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.age < 1) / case count(*) filter (where age < 1) when 0 then 1 else count(*) filter (where age < 1) end::float as "<1",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.age is null) / case count(*) filter (where age is null) when 0 then 1 else count(*) filter (where age is null) end::float as "Sem idade"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--Setor (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = 'COMERCIO') / case count(*) filter (where dp.sector = 'COMERCIO') when 0 then 1 else count(*) filter (where dp.sector = 'COMERCIO') end::float as "Comércio",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = 'SERVICOS') / case count(*) filter (where dp.sector = 'SERVICOS') when 0 then 1 else count(*) filter (where dp.sector = 'SERVICOS') end::float as "Serviços",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = 'CONSTRUCAO CIVIL') / case count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') when 0 then 1 else count(*) filter (where dp.sector = 'CONSTRUCAO CIVIL') end::float as "Construção civil",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = 'AGROPECUARIA') / case count(*) filter (where dp.sector = 'AGROPECUARIA') when 0 then 1 else count(*) filter (where dp.sector = 'AGROPECUARIA') end::float as "Agropecuaria",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = 'INDUSTRIA') / case count(*) filter (where dp.sector = 'INDUSTRIA') when 0 then 1 else count(*) filter (where dp.sector = 'INDUSTRIA') end::float as "Industria",
	count(*) filter (where lr.status = 'ACCEPTED' and dp.sector = '' or dp.sector is null) / case count(*) filter (where dp.sector = '' or dp.sector is null) when 0 then 1 else count(*) filter (where dp.sector = '' or dp.sector is null) end::float as "Sem setor"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1




--Canal (over target population leads)
select to_date,
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel = 'facebook') / case count(*) filter (where lead_marketing_channel = 'facebook') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'facebook') end::float as "facebook",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel = 'organico') / case count(*) filter (where lead_marketing_channel = 'organico') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'organico') end::float as "organico",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel = 'adwords') / case count(*) filter (where lead_marketing_channel = 'adwords') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'adwords') end::float as "google",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel = 'android') / case count(*) filter (where lead_marketing_channel = 'android') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'android') end::float as "mobile",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) / case count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('madeira','prontocomb','peixeurbano','fdex','credita','finpass')) end::float as "grande parceiros",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) / case count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('ARAMAYO','CHARLES','cn','experian','vipac','mobdiq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozzi','andreozziz','VIRGINIA','rentcon','AMBEV')) end::float as "pequenos parceiros",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel in ('taboola','linkedin','twitter','bing')) / case count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('taboola','linkedin','twitter','bing')) end::float as "outras midias",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) / case count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) when 0 then 1 else count(*) filter (where lead_marketing_channel in ('rd','trustpilot','RD Station','blog','email')) end::float as "outros canais",
	count(*) filter (where status = 'ACCEPTED' and lead_marketing_channel = 'NULL') / case count(*) filter (where lead_marketing_channel = 'NULL') when 0 then 1 else count(*) filter (where lead_marketing_channel = 'NULL') end::float as "Sem canal"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),dp.workflow, o.status as status_oferta,lr.status as status_pedido,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end) as t1
where workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1

--compra recompra (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and (is_renew = 0 or is_renew is null)) / case count(*) filter (where is_renew = 0 or is_renew is null) when 0 then 1 else count(*) filter (where is_renew = 0 or is_renew is null) end::float as "Compra",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and is_renew > 0) / case count(*) filter (where is_renew > 0) when 0 then 1 else count(*) filter (where is_renew > 0) end::float as "Recompra"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
--------------------------TABLE CLIENTS
left join (select dp.direct_prospect_id, t1.is_renew
	from direct_prospects dp
	join (select dp.client_id, opt_in_date, count(*) filter (where opt_in_date > t1.request_date) as is_renew
		from direct_prospects dp
		join (select c.client_id, lr.loan_request_id, lr.loan_date as request_date
			from clients c
			join offers o on o.client_id = c.client_id
			join loan_requests lr on lr.offer_id = o.offer_id
			where lr.status = 'ACCEPTED') as t1 on t1.client_id = dp.client_id
		group by 1,2) as t1 on t1.client_id = dp.client_id and t1.opt_in_date = dp.opt_in_date) as isrenew on isrenew.direct_prospect_id = dp.direct_prospect_id
group by 1

--motivo (over target population leads)
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Capital de Giro') / case count(*) filter (where dp.reason_for_loan = 'Capital de Giro') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Capital de Giro') end::float as "Capital de Giro",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Compra de Estoque') / case count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Compra de Estoque') end::float as "Compra de Estoque",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Expansão') / case count(*) filter (where dp.reason_for_loan = 'Expansão') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Expansão') end::float as "Expansão",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Consolidação de Dívidas') / case count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Consolidação de Dívidas') end::float as "Consolidação de Dívidas",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Compra de Equipamentos') / case count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Compra de Equipamentos') end::float as "Compra de Equipamentos",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Reforma') / case count(*) filter (where dp.reason_for_loan = 'Reforma') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Reforma') end::float as "Reforma",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Outros') / case count(*) filter (where dp.reason_for_loan = 'Outros') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Outros') end::float as "Outros",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Marketing e Vendas') / case count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Marketing e Vendas') end::float as "Marketing e Vendas",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Pessoal') / case count(*) filter (where dp.reason_for_loan = 'Pessoal') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Pessoal') end::float as "Pessoal",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and dp.reason_for_loan = 'Uso Pessoal') / case count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') when 0 then 1 else count(*) filter (where dp.reason_for_loan = 'Uso Pessoal') end::float as "Uso Pessoal",
	count(*) filter (where workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido') and (dp.reason_for_loan = '' or dp.reason_for_loan is null)) / case count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) when 0 then 1 else count(*) filter (where dp.reason_for_loan = '' or dp.reason_for_loan is null) end::float as "Sem motivo"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa')
group by 1


--NEGACAO
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
--MOTIVO DE NEGAÇÃO LEAD -> OFFER - mensal
select case when dp.workflow in ('Negado Bizu','Negado ProposalScore','Negado Restricao','Negado Score','Negado Score Serasa','Negado SCR CNPJ','Negado SCR CPF','Negado Serasa','NegadoAlavancagem') then 'Nivel de risco' 
	when dp.workflow in ('Negado Baixada','Negado CNAE','Negado Faturamento','Negado Idade','Negado Suspensa') then 'Publico alvo'
	when dp.workflow in ('Duplicata','Erro CNPJ Neoway','Erro Neoway','Negado Analise','Negado Comite','Negado Duplicata','Negado Fraude','Negado MEI','Negado Pedido Duplicado','Negado Renovação','Negado Veículo','NegadoDesproporcional') then 'Restrição interna' 
	else dp.workflow end,
	count(*) filter (where to_char(dp.opt_in_date,'yyyy') = '2017') "2017",
	count(*) filter (where to_char(dp.opt_in_date,'yyyy') = '2018') "2018-YTD",
	count(*) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-10') "2018-10",
	count(*) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-11') "2018-11"
--------------------------
from direct_prospects dp
left join clients c on c.client_id = dp.client_id
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is null and dp.workflow not in ('Oferta Enviada','Enviar Proposta','Proposta Enviada','Analise Renovacao','Analise Comite','Investigacao','Formulario Preenchido','Nao Processado','Perdido','PedidoPerdido')
group by 1

--MOTIVO DE NEGAÇÃO
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	case when dp.workflow in ('Negado SCR CNPJ',	'Negado SCR CPF','Negado Restricao','Negado Serasa') then 'd) Restritivo externo' 
		when dp.workflow = 'Negado Fraude' then 'g) Suspeita de fraude'
		when dp.workflow = 'Negado Faturamento' then 'a) Faturamento'
		when dp.workflow = 'Negado Idade' then 'b) Idade'
		when dp.workflow in ('NegadoDesproporcional','Negado Pedido Duplicado','Negado Veículo','Negado Renovação','Negado Comite','Negado Analise') then 'f) Comite' 
		when dp.workflow in ('Erro CNPJ Neoway','Negado Suspensa','Erro Neoway','Negado Baixada','Negado CNAE','Negado Duplicata','Duplicata','Perdido') then 'c) Publico alvo (outros)' 
		when dp.workflow in ('Negado ProposalScore','Negado Bizu','NegadoAlavancagem','Negado MEI','Negado Score Serasa','Negado Score') then 'e) Nivel de risco'
		when dp.workflow in ('PedidoRejeitado','PedidoCancelado','OfertaCancelada') or o.status in ('Cancelada','Substituida','PedidoRejeitado') or lr.status in ('Cancelado','Substituido') then 'i) Rejeitado'
		when dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') then 'h) Aprovado'
	else dp.workflow end as motivo,
	count(*) as units
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where dp.workflow not in ('Analise Renovacao','Analise Comite','Formulario Preenchido','Nao Processado')
group by 1,2
order by 1,2

--ZOOM APROVADO - fechamento
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	case when dp.workflow in ('Aprovado','PedidoAprovado') then 'a) Pedido aprovado'
		when (dp.workflow in ('PedidoPerdido','PedidoExpirado') or lr.status in ('LOST','Expirado')) and (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) then 'b) Pedido perdido com doc'
		when (dp.workflow in ('PedidoPerdido','PedidoExpirado') or lr.status in ('LOST','Expirado')) and lr.loan_request_id is not null then 'c) Pedido perdido sem doc'
		when dp.workflow in ('OfertaExpirada','OfertaPerdida') then 'd) Oferta perdida'
	else 'e) Aguardando definição' end as motivo,
	count(*) as units
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where dp.workflow in ('Aprovado','PedidoPerdido','OfertaExpirada','PedidoAprovado','OfertaPerdida','PedidoExpirado','Proposta Enviada','Em Negociacao','Analise Renovacao','Investigacao') and coalesce(o.status,'Sem oferta') not in ('Cancelada','Substituida','PedidoRejeitado') and coalesce(lr.status,'Sem pedido') not in ('Cancelado','Substituido')
group by 1,2
order by 1,2

--MOTIVO DESISTENCIA OFFER
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
----------------------------------------------------------------
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	coalesce(replace(case when lr.razao_perda = 'Sem conta PJ' or o.razao_perda = 'Sem conta PJ' then 'Sem conta PJ'
		when lr.razao_perda in ('Taxa alta','CET alto') or o.razao_perda in ('Taxa alta','CET alto') then 'Taxa ou CET alta'
		when lr.razao_perda = 'Pegou com banco/outro' or o.razao_perda = 'Pegou com banco/outro' then 'Pegou com outro banco'
		when lr.razao_perda = 'Burocracia de docs' or o.razao_perda = 'Burocracia de docs' then 'Burocracia Docs'
		when lr.razao_perda = 'Prazo curto' or o.razao_perda = 'Prazo curto' then 'Prazo curto'
		when lr.razao_perda = 'Cotação' or o.razao_perda = 'Cotação' then 'Cotação'
		when lr.razao_perda is null then o.razao_perda 
	else lr.razao_perda end,'Outro motivo','Outra razão'),'Sem motivo') as motivo,
	count(*) units
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
where o.direct_prospect_id is not null and o.status in ('Perdida','PedidoPerdido') and not (lr.analista_responsavel in ('Bruno Lourenço','Felipe Viana','Rodrigo Avila') and lr.razao_perda = 'Outra razão' and o.razao_perda is null) 
group by 1,2
order by 1,2

--valor
select coalesce(case when lr.razao_perda = 'Sem conta PJ' or o.razao_perda = 'Sem conta PJ' then 'Sem conta PJ'
		when lr.razao_perda in ('Taxa alta','CET alto') or o.razao_perda in ('Taxa alta','CET alto') then 'Taxa ou CET alta'
		when lr.razao_perda = 'Pegou com banco/outro' or o.razao_perda = 'Pegou com banco/outro' then 'Pegou com outro banco'
		when lr.razao_perda = 'Burocracia de docs' or o.razao_perda = 'Burocracia de docs' then 'Burocracia Docs'
		when lr.razao_perda = 'Prazo curto' or o.razao_perda = 'Prazo curto' then 'Prazo curto'
		when lr.razao_perda = 'Cotação' or o.razao_perda = 'Cotação' then 'Cotação'
		when lr.razao_perda is null then o.razao_perda 
		else lr.razao_perda end,'Sem motivo'),
	sum(lr.value)
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
where (cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) and lr.status = 'LOST'
group by 1


--Negação mesa
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as cohort_lead,
	case when mr.nome = 'Complexidade Estrutural' then 'Structure complexity'
		when mr.nome = 'Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)' then 'Bad payment record (Experian/SCR)'
		when mr.nome = 'Baixo Faturamento Comprovado' then 'Bad payment record (bank stmts)'
		when mr.nome = 'Mau Pagamento ao Mercado (Extrato)' then 'Low income (bank stmts)'
		when mr.nome in ('Política de Crédito (Score Serasa Baixo)','Política de Crédito (Score Serasa Baixo na análise de Documentação )') then 'Policy: low Experian score'
		when mr.nome = 'Queda do Faturamento' then 'Income descrease'
		when mr.nome = 'Suspeita de Fraude' then 'Fraud suspicion'
		when mr.nome = 'Política de Crédito (Alteração no Contrato Social)' then 'Policy: social contract change'
		when mr.nome = 'Falta de Capacidade de Pagamento' then 'Low margin'
		when mr.nome = 'Política de Crédito (Atividade Fim da Empresa)' then 'Policy: company activity'
		when mr.nome = 'Política de Crédito (Objetivo do Empréstimo)' then 'Policy: money use'
		when mr.nome = 'Negou Informações/Anuência' then 'Info/consent denial'
		when mr.nome = 'Endividamento Elevado (Extrato/Crédito Recente)' then 'High indebtness due to recent borrow (bank stmts)'
		when mr.nome = 'LT Baixo' then 'Low LT score'
		when mr.nome = 'Política de Crédito (Quadro Societário)' then 'Policy: shareholder change'
		when mr.nome = 'Endividamento Elevado (Comprovado Baixo Faturamento)' then 'High indebtness due to low income (bank stmts)'
		when mr.nome = 'Problemas Com a Justiça' then 'Justice issues'
		when mr.nome = 'Empresa não possui conta PJ' then 'No bank account for company'
		when mr.nome = 'Documentação Irregular' then 'Documentation issues'
		when mr.nome = 'Política de Crédito (Empresa com menos de 1 ano de operação)' then 'Policy: less than 1 yo'
	else coalesce(mr.nome,'Sem motivo') end as motivo,
	count(lr.loan_request_id) as units
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
left join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id
where lr.status in ('Cancelado','REJECTED')
group by 1,2
order by 1,2


--AGRUPAMENTO COM COMBINAÇÃO - mensal
select coalesce(motivo,'Sem motivo') as motivo_rejeicao_mesa,
--	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end) filter (where to_char(dp.opt_in_date,'yyyy') = '2017') as "2017",
--	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end) filter (where to_char(dp.opt_in_date,'yyyy') = '2018') as "2018",
--	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-10') as "2018-10",
--	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-11') as "2018-11",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2017') as "2017",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2018') as "2018",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-10') as "2018-10",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') = '2018-11') as "2018-11"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
--------------------------MOTIVOS REJEICAO
left join(select loan_request_id,string_agg(distinct mr.nome,' + ') as motivo
	from loan_requests lr 
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join (select case when nome in ('Mau Pagamento ao Mercado (Extrato)','Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)','Problemas Com a Justiça') then 'Mau pgto./Prob. Jud.'
		when nome in ('Baixo Faturamento Comprovado','Queda do Faturamento') then 'Faturamento' 
		when nome in ('Endividamento Elevado (Extrato/Crédito Recente)','Endividamento Elevado (Comprovado Baixo Faturamento)','Falta de Capacidade de Pagamento') then 'Endividamento' 
		when nome in ('Política de Crédito (Score Serasa Baixo na análise de Documentação )','Política de Crédito (Alteração no Contrato Social)','Política de Crédito (Atividade Fim da Empresa)','Política de Crédito (Objetivo do Empréstimo)','Política de Crédito (Score Serasa Baixo)','Política de Crédito (Empresa com menos de 1 ano de operação)','Política de Crédito (Quadro Societário)') then 'Politica' 
		else nome end as nome,id from motivos_rejeicao) mr on mr.id = lrm.motivosrejeicao_id
	group by 1) as motivos on motivos.loan_request_id = lr.loan_request_id
where lr.status in ('Cancelado','REJECTED','Perdido')
group by 1
order by 5 desc

--AGRUPAMENTO COM COMBINAÇÃO - fechamento
select coalesce(motivo,'Sem motivo') as motivo_rejeicao_mesa,
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2017') "2017",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2018') "2018",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')) "Q1_2018",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')) "Q2_2018",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')) "Q3_2018",
	count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')) "Q4_2018"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
--------------------------MOTIVOS REJEICAO
left join(select loan_request_id,string_agg(distinct mr.nome,' + ') as motivo
	from loan_requests lr 
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join (select case when nome in ('Mau Pagamento ao Mercado (Extrato)','Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)','Problemas Com a Justiça') then 'Mau pgto./Prob. Jud.'
		when nome in ('Baixo Faturamento Comprovado','Queda do Faturamento') then 'Faturamento' 
		when nome in ('Endividamento Elevado (Extrato/Crédito Recente)','Endividamento Elevado (Comprovado Baixo Faturamento)','Falta de Capacidade de Pagamento') then 'Endividamento' 
		when nome in ('Política de Crédito (Score Serasa Baixo na análise de Documentação )','Política de Crédito (Alteração no Contrato Social)','Política de Crédito (Atividade Fim da Empresa)','Política de Crédito (Objetivo do Empréstimo)','Política de Crédito (Score Serasa Baixo)','Política de Crédito (Empresa com menos de 1 ano de operação)','Política de Crédito (Quadro Societário)') then 'Politica' 
		else nome end as nome,id from motivos_rejeicao) mr on mr.id = lrm.motivosrejeicao_id
	group by 1) as motivos on motivos.loan_request_id = lr.loan_request_id
where lr.status in ('Cancelado','REJECTED','Perdido')
group by 1
order by 7 desc

--AGRUPAMENTO COM COMBINAÇÃO - total
select coalesce(motivos.motivo,'Sem motivo') as motivo_rejeicao_mesa,
--	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end),
	count(distinct lr.loan_request_id)
--------------------------
from loan_requests lr
--------------------------MOTIVOS REJEICAO
left join(select loan_request_id,string_agg(distinct mr.nome,' + ') as motivo
	from loan_requests lr 
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join (select case when nome in ('Mau Pagamento ao Mercado (Extrato)','Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)','Problemas Com a Justiça') then 'Mau pgto./Prob. Jud.'
		when nome in ('Baixo Faturamento Comprovado','Queda do Faturamento') then 'Faturamento' 
		when nome in ('Endividamento Elevado (Extrato/Crédito Recente)','Endividamento Elevado (Comprovado Baixo Faturamento)','Falta de Capacidade de Pagamento') then 'Endividamento' 
		when nome in ('Política de Crédito (Score Serasa Baixo na análise de Documentação )','Política de Crédito (Alteração no Contrato Social)','Política de Crédito (Atividade Fim da Empresa)','Política de Crédito (Objetivo do Empréstimo)','Política de Crédito (Score Serasa Baixo)','Política de Crédito (Empresa com menos de 1 ano de operação)','Política de Crédito (Quadro Societário)') then 'Politica' 
		else nome end as nome,id from motivos_rejeicao) mr on mr.id = lrm.motivosrejeicao_id
	group by 1) as motivos on motivos.loan_request_id = lr.loan_request_id
where lr.status in ('Cancelado','REJECTED','Perdido')
group by 1
order by 2 desc

--SEM AGRUPAMENTO - total
select coalesce(motivos.nome,'Sem motivo') as motivo_rejeicao_mesa,
	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end),
	count(distinct lr.loan_request_id)
--------------------------
from loan_requests lr
--------------------------MOTIVOS REJEICAO
left join(select loan_request_id, mr.nome
	from loan_requests lr
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id) as motivos on motivos.loan_request_id = lr.loan_request_id
where lr.status in ('Cancelado','REJECTED','Perdido')
group by 1
order by 2 desc


--SEM AGRUPAMENTO COM COMBINAÇÃO - fechamento
select case when row_number < 11 then motivo_rejeicao_mesa else 'Outro' end as motivo_rejeicao_mesa,
	sum("2017") as "2017",sum("2018") as "2018",sum("Q1_2018") as "Q1_2018",sum("Q2_2018") as "Q2_2018",sum("Q3_2018") as "Q3_2018",sum("Q4_2018") as "Q4_2018"
from(select motivo_rejeicao_mesa,row_number() over (order by "Q4_2018" desc),"2017","2018","Q1_2018","Q2_2018","Q3_2018","Q4_2018"
		from (select coalesce(motivos.motivo,'Sem motivo') as motivo_rejeicao_mesa,
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2017') "2017",
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy') = '2018') "2018",
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')) "Q1_2018",
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')) "Q2_2018",
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')) "Q3_2018",
		count(distinct lr.loan_request_id) filter (where to_char(dp.opt_in_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')) "Q4_2018"
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	--------------------------MOTIVOS REJEICAO
	left join(select loan_request_id,string_agg(distinct mr.nome,' + ') as motivo
		from loan_requests lr 
		join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
		join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id
		group by 1) as motivos on motivos.loan_request_id = lr.loan_request_id
	where lr.status in ('Cancelado','REJECTED','Perdido')
	group by 1
	order by 7 desc) as t1) as t2
group by 1
order by 7 desc

--AGRUPADO SEM COMBINAÇÃO - total
select coalesce(motivos.motivo,'Sem motivo') as motivo_rejeicao_mesa,
	sum(case when lr.valor_solicitado is null then lr.value else lr.valor_solicitado end),
	count(*)
--------------------------
from direct_prospects dp
left join clients c on c.client_id = dp.client_id
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select o.* ,count_neg from offers o join (select direct_prospect_id,min(offer_id) as min_id,count(*) as count_neg from offers group by 1) as initial_off on initial_off.min_id = o.offer_id) as initialoff on initialoff.direct_prospect_id = dp.direct_prospect_id
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------MOTIVOS REJEICAO
left join(select distinct loan_request_id,mr.nome as motivo
	from loan_requests lr 
	join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
	join (select case when nome in ('Mau Pagamento ao Mercado (Extrato)','Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)','Problemas Com a Justiça') then 'Mau pgto./Prob. Jud.'
		when nome in ('Baixo Faturamento Comprovado','Queda do Faturamento') then 'Faturamento' 
		when nome in ('Endividamento Elevado (Extrato/Crédito Recente)','Endividamento Elevado (Comprovado Baixo Faturamento)','Falta de Capacidade de Pagamento') then 'Endividamento' 
		when nome in ('Política de Crédito (Score Serasa Baixo na análise de Documentação )','Política de Crédito (Alteração no Contrato Social)','Política de Crédito (Atividade Fim da Empresa)','Política de Crédito (Objetivo do Empréstimo)','Política de Crédito (Score Serasa Baixo)','Política de Crédito (Empresa com menos de 1 ano de operação)','Política de Crédito (Quadro Societário)') then 'Politica' 
		else nome end as nome,id from motivos_rejeicao) mr on mr.id = lrm.motivosrejeicao_id) as motivos on motivos.loan_request_id = lr.loan_request_id
where lr.status in ('Cancelado','REJECTED','Perdido')
group by 1
order by 3 desc

--PONDERADO
select motivo_rejeicao_mesa,
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy') = '2017'),0) "2017",
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy') = '2018'),0) "2018",
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')),0) "Q1_2018",
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')),0) "Q2_2018",
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')),0) "Q3_2018",
	coalesce(sum(coalesce(fator,count)) filter (where to_char(opt_in_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')),0) "Q4_2018"
from(
	select coalesce(motivos.nome,'Sem motivo') as motivo_rejeicao_mesa, opt_in_date,fator,count(*)
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	--------------------------MOTIVOS REJEICAO
	left join(select loan_request_id, mr.nome,fator
		from loan_requests lr
		join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
		join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id
		join (select loanrequest_id,1/count(*)::float as fator from loan_requests_motivos_rejeicao group by 1) as t1 on t1.loanrequest_id = lr.loan_request_id) as motivos on motivos.loan_request_id = lr.loan_request_id
	where lr.status in ('Cancelado','REJECTED','Perdido')
	group by 1,2,3) as t1
group by 1 
order by 7 desc

select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) filter (where mr.nome = 'Complexidade Estrutural')/count(distinct lr.loan_request_id)::float as "Structure complexity",
	count(*) filter (where mr.nome = 'Mau Pagamento ao Mercado (Anotações Restritivas na PF e/ou PJ e/ou SCR)')/count(distinct lr.loan_request_id)::float as "Bad payment record (Experian/SCR)",
	count(*) filter (where mr.nome = 'Baixo Faturamento Comprovado')/count(distinct lr.loan_request_id)::float "Low income (bank stmts)",
	count(*) filter (where mr.nome = 'Mau Pagamento ao Mercado (Extrato)')/count(distinct lr.loan_request_id)::float "Bad payment record (bank stmts)",
	count(*) filter (where mr.nome in ('Política de Crédito (Score Serasa Baixo)','Política de Crédito (Score Serasa Baixo na análise de Documentação )'))/count(distinct lr.loan_request_id)::float as "Policy: low Experian score",
	count(*) filter (where mr.nome = 'Queda do Faturamento')/count(distinct lr.loan_request_id)::float as "Income descrease",
	count(*) filter (where mr.nome = 'Suspeita de Fraude')/count(distinct lr.loan_request_id)::float "Fraud suspicion",
	count(*) filter (where mr.nome = 'Política de Crédito (Alteração no Contrato Social)')/count(distinct lr.loan_request_id)::float "Policy: social contract change",
	count(*) filter (where mr.nome = 'Falta de Capacidade de Pagamento')/count(distinct lr.loan_request_id)::float "Low margin",
	count(*) filter (where mr.nome = 'Política de Crédito (Atividade Fim da Empresa)')/count(distinct lr.loan_request_id)::float as "Policy: company activity",
	count(*) filter (where mr.nome = 'Política de Crédito (Objetivo do Empréstimo)')/count(distinct lr.loan_request_id)::float as "Policy: money use",
	count(*) filter (where mr.nome = 'Negou Informações/Anuência')/count(distinct lr.loan_request_id)::float "Info/consent denial",
	count(*) filter (where mr.nome = 'Endividamento Elevado (Extrato/Crédito Recente)')/count(distinct lr.loan_request_id)::float "High indebtness due to recent borrow (bank stmts)",
	count(*) filter (where mr.nome = 'LT Baixo')/count(distinct lr.loan_request_id)::float "Low LT score",
	count(*) filter (where mr.nome = 'Política de Crédito (Quadro Societário)')/count(distinct lr.loan_request_id)::float "Policy: shareholder change",
	count(*) filter (where mr.nome = 'Endividamento Elevado (Comprovado Baixo Faturamento)')/count(distinct lr.loan_request_id)::float as "High indebtness due to low income (bank stmts)",
	count(*) filter (where mr.nome = 'Problemas Com a Justiça')/count(distinct lr.loan_request_id)::float "Justice issues",
	count(*) filter (where mr.nome = 'Empresa não possui conta PJ')/count(distinct lr.loan_request_id)::float "No bank account for company",
	count(*) filter (where mr.nome = 'Documentação Irregular')/count(distinct lr.loan_request_id)::float as "Documentation issues",
	count(*) filter (where mr.nome = 'Política de Crédito (Empresa com menos de 1 ano de operação)')/count(distinct lr.loan_request_id)::float "Policy: less than 1 yo",
	count(*) filter (where lrm.loanrequest_id is null)/count(distinct lr.loan_request_id)::float as "Empty"	
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join loan_requests_motivos_rejeicao lrm on lrm.loanrequest_id = lr.loan_request_id
left join motivos_rejeicao mr on mr.id = lrm.motivosrejeicao_id
where lr.status in ('Cancelado','REJECTED')
group by 1


--aprovado automaticamente
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
--Evolutivo efetivação automática
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) as "Total Loan (#)",
	count(*) filter (where aa.directprospect_id is not null) as "Low Ticket Loan (#)",
	sum(lr.value) as "Loan Amount ($)",
	sum(lr.value) filter (where aa.directprospect_id is not null) as "Low Ticket Amount ($)"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
left join (select dpt.directprospect_id from direct_prospects_tag dpt join tags t on t.id = dpt.tag_id where t.nome = 'AA<=10-v1') as aa on aa.directprospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1

select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),count(*)
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join (select dpt.directprospect_id from direct_prospects_tag dpt join tags t on t.id = dpt.tag_id where t.nome = 'AA<=10-v1') as aa on aa.directprospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1

--FPD e SPD de LOW TICKET
select case when parcemaberto = 1 then 
			case when max_late_pmt1 > 30 then 'Current FPD 30' 
			when max_late_pmt1 > 15 then 'Current FPD 15' 
			when max_late_pmt1 > 7 then 'Current FPD 7' 
			when max_late_pmt1 > 0 then 'Current FPD 1' else 'To be due' end
		when parcemaberto = 2 then 
			case when max_late_pmt2 > 30 then 'Current SPD 30' 
			when max_late_pmt2 > 15 then 'Current SPD 15' 
			when max_late_pmt2 > 7 then 'Current SPD 7' 
			when max_late_pmt2 > 0 then 'Current SPD 1' else 'To be due' end
		else 
			case when max_late_pmt1 > 30 then 'Previuos FPD 30' 
			when max_late_pmt1 > 15 then 'Previuos FPD 15' 
			when max_late_pmt1 > 7 then 'Previuos FPD 7' 
			when max_late_pmt1 > 0 then 'Previuos FPD 1'
			when max_late_pmt2 > 30 then 'Previuos SPD 30' 
			when max_late_pmt2 > 15 then 'Previuos SPD 15' 
			when max_late_pmt2 > 7 then 'Previuos SPD 7' 
			when max_late_pmt2 > 0 then 'Previuos SPD 1' else 'Neither' end
		end,
	count(*) filter (where to_char(loan_date,'yyyy') = '2018') "2018",
	count(*) filter (where to_char(loan_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')) "Q1_2018",
	count(*) filter (where to_char(loan_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')) "Q2_2018",
	count(*) filter (where to_char(loan_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')) "Q3_2018",
	count(*) filter (where to_char(loan_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')) "Q4_2018",
	sum(case when parcemaberto = 1 then EAD_FPD when parcemaberto = 2 then EAD_SPD when max_late_pmt1 > 0 then EAD_FPD when max_late_pmt2 > 0 then EAD_SPD end) filter (where to_char(loan_date,'yyyy') = '2018') "2018",
	sum(case when parcemaberto = 1 then EAD_FPD when parcemaberto = 2 then EAD_SPD when max_late_pmt1 > 0 then EAD_FPD when max_late_pmt2 > 0 then EAD_SPD end) filter (where to_char(loan_date,'yyyy-MM') in ('2018-01','2018-02','2018-03')) "Q1_2018",
	sum(case when parcemaberto = 1 then EAD_FPD when parcemaberto = 2 then EAD_SPD when max_late_pmt1 > 0 then EAD_FPD when max_late_pmt2 > 0 then EAD_SPD end) filter (where to_char(loan_date,'yyyy-MM') in ('2018-04','2018-05','2018-06')) "Q2_2018",
	sum(case when parcemaberto = 1 then EAD_FPD when parcemaberto = 2 then EAD_SPD when max_late_pmt1 > 0 then EAD_FPD when max_late_pmt2 > 0 then EAD_SPD end) filter (where to_char(loan_date,'yyyy-MM') in ('2018-07','2018-08','2018-09')) "Q3_2018",
	sum(case when parcemaberto = 1 then EAD_FPD when parcemaberto = 2 then EAD_SPD when max_late_pmt1 > 0 then EAD_FPD when max_late_pmt2 > 0 then EAD_SPD end) filter (where to_char(loan_date,'yyyy-MM') in ('2018-10','2018-11','2018-12')) "Q4_2018"
--------------------------
FROM direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join (select dpt.directprospect_id from direct_prospects_tag dpt join tags t on t.id = dpt.tag_id where t.nome = 'AA<=10-v1') as aa on aa.directprospect_id = dp.direct_prospect_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
	count(distinct t2.plano_id) as count_renegotiation,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
left join(Select fpp.emprestimo_id,sum(fp.cobrado) as EAD_FPD, sum(fp.cobrado) filter (where fp.numero > 1) as EAD_SPD
	from (select emprestimo_id, min(id) id from financeiro_planopagamento group by 1) fpp
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
	group by 1) as EAD on EAD.emprestimo_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and ParcEmAberto is not null and case when parcemaberto = 1 then max_late_pmt1 >= 0 else true end
group by 1	


--FPD e SPD de LOW TICKET 2 - UNIT
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy'),
	--Current Unit FPD
	count(*) filter (where parcemaberto = 1 and max_late_pmt1 > 30) u_current_FPD_30,
	count(*) filter (where parcemaberto = 1 and max_late_pmt1 between 16 and 30) u_current_FPD_15_30,
	count(*) filter (where parcemaberto = 1 and max_late_pmt1 between 8 and 15) u_current_FPD_7_15,
	count(*) filter (where parcemaberto = 1 and max_late_pmt1 between 1 and 7) u_current_FPD_1_7,
	--Previous Unit FPD
	count(*) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 > 30) u_previous_FPD_30,
	count(*) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 16 and 30) u_previous_FPD_15_30,
	count(*) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 8 and 15) u_previous_FPD_7_15,
	count(*) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 1 and 7) u_previous_FPD_1_7,
	--Current Unit SPD
	count(*) filter (where parcemaberto = 2 and max_late_pmt2 > 30) u_current_SPD_30,
	count(*) filter (where parcemaberto = 2 and max_late_pmt2 between 16 and 30) u_current_SPD_15_30,
	count(*) filter (where parcemaberto = 2 and max_late_pmt2 between 8 and 15) u_current_SPD_7_15,
	count(*) filter (where parcemaberto = 2 and max_late_pmt2 between 1 and 7) u_current_SPD_1_7,
	--Previous Unit SPD
	count(*) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 > 30 and max_late_pmt1 <= 0) u_previous_SPD_30,
	count(*) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 16 and 30 and max_late_pmt1 <= 0) u_previous_SPD_15_30,
	count(*) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 8 and 15 and max_late_pmt1 <= 0) u_previous_SPD_7_15,
	count(*) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 1 and 7 and max_late_pmt1 <= 0) u_previous_SPD_1_7,
	--Total
	count(*) as total_units
--------------------------
FROM direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join (select dpt.directprospect_id from direct_prospects_tag dpt join tags t on t.id = dpt.tag_id where t.nome = 'AA<=10-v1') as aa on aa.directprospect_id = dp.direct_prospect_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
	count(distinct t2.plano_id) as count_renegotiation,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and case when parcemaberto = 1 then max_late_pmt1 >= 0 else true end
group by 1	


--FPD e SPD de LOW TICKET 2 - VALUE
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy'),
	--Current Value FPD
	sum(EAD_FPD) filter (where parcemaberto = 1 and max_late_pmt1 > 30) v_current_FPD_30,
	sum(EAD_FPD) filter (where parcemaberto = 1 and max_late_pmt1 between 16 and 30) v_current_FPD_15_30,
	sum(EAD_FPD) filter (where parcemaberto = 1 and max_late_pmt1 between 8 and 15) v_current_FPD_7_15,
	sum(EAD_FPD) filter (where parcemaberto = 1 and max_late_pmt1 between 1 and 7) v_current_FPD_1_7,
	--Previous Value FPD
	sum(EAD_FPD) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 > 30 and max_late_pmt1 <= 0) v_previous_FPD_30,
	sum(EAD_FPD) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 16 and 30 and max_late_pmt1 <= 0) v_previous_FPD_15_30,
	sum(EAD_FPD) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 8 and 15 and max_late_pmt1 <= 0) v_previous_FPD_7_15,
	sum(EAD_FPD) filter (where coalesce(parcemaberto,0) > 1 and max_late_pmt1 between 1 and 7 and max_late_pmt1 <= 0) v_previous_FPD_1_7,
	--Current Value SPD
	sum(EAD_SPD) filter (where parcemaberto = 2 and max_late_pmt2 > 30) v_current_SPD_30,
	sum(EAD_SPD) filter (where parcemaberto = 2 and max_late_pmt2 between 16 and 30) v_current_SPD_15_30,
	sum(EAD_SPD) filter (where parcemaberto = 2 and max_late_pmt2 between 8 and 15) v_current_SPD_7_15,
	sum(EAD_SPD) filter (where parcemaberto = 2 and max_late_pmt2 between 1 and 7) v_current_SPD_1_7,
	--Previous Value SPD
	sum(EAD_SPD) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 > 30 and max_late_pmt1 <= 0) v_previous_SPD_30,
	sum(EAD_SPD) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 16 and 30 and max_late_pmt1 <= 0) v_previous_SPD_15_30,
	sum(EAD_SPD) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 8 and 15 and max_late_pmt1 <= 0) v_previous_SPD_7_15,
	sum(EAD_SPD) filter (where coalesce(parcemaberto,0) > 2 and max_late_pmt2 between 1 and 7 and max_late_pmt1 <= 0) v_previous_SPD_1_7,
	--Total
	sum(EAD_FPD) as total_value
--------------------------
FROM direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join (select dpt.directprospect_id from direct_prospects_tag dpt join tags t on t.id = dpt.tag_id where t.nome = 'AA<=10-v1') as aa on aa.directprospect_id = dp.direct_prospect_id
--------------------------TABLE DPD SOFT
left join(select lr.loan_request_id,
	count(distinct t2.plano_id) as count_renegotiation,
	min(fp.numero) filter (where fp.pago_em is null) ParcEmAberto,
	max(case when fp.numero <> 1 then null else case when ParcSubst = 1 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt1,
	max(case when fp.numero <> 2 then null else case when ParcSubst = 2 and VencParcSubst is not null then case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end else case when fp.pago_em is null and current_date >= fp.vencimento_em then current_date - fp.vencimento_em else fp.pago_em - fp.vencimento_em end end end) as max_late_pmt2
	from loan_requests lr
	join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
	join financeiro_parcela fp on fp.plano_id = fpp.id
	left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
		from financeiro_parcela fp
		join financeiro_planopagamento fpp on fpp.id = fp.plano_id
		where fp.status = 'Cancelada'
		group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
	where fp.status not in ('Cancelada','EmEspera')
	group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
left join(Select fpp.emprestimo_id,sum(fp.cobrado) as EAD_FPD, sum(fp.cobrado) filter (where fp.numero > 1) as EAD_SPD
	from (select emprestimo_id, min(id) id from financeiro_planopagamento group by 1) fpp
	join financeiro_parcela fp on fp.plano_id = fpp.id
	join loan_requests lr on lr.loan_request_id = fpp.emprestimo_id
	group by 1) as EAD on EAD.emprestimo_id = lr.loan_request_id
where lr.status = 'ACCEPTED' and case when parcemaberto = 1 then max_late_pmt1 >= 0 else true end
group by 1


--TAXA DE CONVERSÃO
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
--TOTAL
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	count(*) as "Leads units",
	sum(dp.amount_requested) "Leads value",
	count(*) filter (where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null) as "Qualificados units",
	sum(dp.amount_requested) filter (where (dp.serasa_4 is not null and dp.bizu2_score >=300) or o.direct_prospect_id is not null) as "Qualificados value",
	count(*) filter (where o.direct_prospect_id is not null) as "Ofertas units",
	sum(o.max_value) filter (where o.direct_prospect_id is not null) "Ofertas value",
	count(*) filter (where lr.loan_request_id is not null) as "Pedidos units",
	sum(lr.valor_solicitado) filter (where lr.loan_request_id is not null) as "Pedidos value",
	count(*) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) "Pedidos completos units",
	sum(lr.valor_solicitado) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) as "Pedidos completos value",
	count(*) filter (where lr.status = 'ACCEPTED') as "Emprestimos units",
	sum(lr.value) filter (where lr.status = 'ACCEPTED') "Emprestimos value"
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
group by 1

--TEMPO DE FECHAMENTO
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy') as mes,
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '3 days') as "Up to 3",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '5 days') as "Up to 5",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '7 days') as "Up to 7",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '10 days') as "Up to 10",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '14 days') as "Up to 14",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '20 days') as "Up to 20",
	count(*) filter (where lr.loan_date - dp.opt_in_date <= interval '30 days') as "Up to 30",
	count(*) as "Over 30"
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
where lr.status = 'ACCEPTED'
group by 1



