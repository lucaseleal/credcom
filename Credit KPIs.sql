--Bizu
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where bizu_score >= 850) as "A+",
	avg(dp.bizu_score) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(dp.bizu_score) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(dp.bizu_score) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(dp.bizu_score) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(dp.bizu_score) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(dp.bizu_score) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(dp.bizu_score) filter (where bizu_score < 300) as "E",
	avg(dp.bizu_score) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(dp.bizu_score) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(dp.bizu_score) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(dp.bizu_score) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(dp.bizu_score) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(dp.bizu_score) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(dp.bizu_score) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(dp.bizu_score) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where dp.month_revenue > 400000) as ">400k",
	avg(dp.bizu_score) filter (where dp.month_revenue > 200000) as ">200k",
	avg(dp.bizu_score) filter (where dp.month_revenue > 80000) as ">80k",
	avg(dp.bizu_score) filter (where dp.month_revenue > 40000) as ">40k",
	avg(dp.bizu_score) filter (where dp.month_revenue > 10000) as ">10k",
	avg(dp.bizu_score) filter (where dp.month_revenue < 10000) as "<10k",
	avg(dp.bizu_score) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where dp.age > 20) as ">20",
	avg(dp.bizu_score) filter (where dp.age > 10) as ">10",
	avg(dp.bizu_score) filter (where dp.age > 5) as ">5",
	avg(dp.bizu_score) filter (where dp.age > 3) as ">3",
	avg(dp.bizu_score) filter (where dp.age > 2) as ">2",
	avg(dp.bizu_score) filter (where dp.age > 1) as ">1",
	avg(dp.bizu_score) filter (where dp.age < 1) as "<1",
	avg(dp.bizu_score) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(dp.bizu_score) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(dp.bizu_score) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(dp.bizu_score) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(dp.bizu_score) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(dp.bizu_score) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(dp.bizu_score) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(bizu_score) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(bizu_score) filter (where lead_marketing_channel = 'organico') "organico",
	avg(bizu_score) filter (where lead_marketing_channel = 'adwords') "google",
	avg(bizu_score) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(bizu_score) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(bizu_score) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),dp.bizu_score,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1

--Serasa
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score >= 850) as "A+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score < 300) as "E",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue > 400000) as ">400k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue > 200000) as ">200k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue > 80000) as ">80k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue > 40000) as ">40k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue > 10000) as ">10k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue < 10000) as "<10k",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 20) as ">20",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 10) as ">10",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 5) as ">5",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 3) as ">3",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 2) as ">2",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age > 1) as ">1",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age < 1) as "<1",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(case when serasa_coleta is null then o.rating::int else serasa_coleta end) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(serasa) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(serasa) filter (where lead_marketing_channel = 'organico') "organico",
	avg(serasa) filter (where lead_marketing_channel = 'adwords') "google",
	avg(serasa) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(serasa) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(serasa) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),case when serasa_coleta is null then o.rating::int else serasa_coleta end as serasa,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	--------------------------TABLE SERASA SCORE
	left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
		from credito_coleta cc
		join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
			from credito_coleta cc
			join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
			left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
				from direct_prospects dp
				left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
				left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
				left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
				group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
			left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
			left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
			where tipo = 'Serasa'
			group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
	where lr.status = 'ACCEPTED') as t1
group by 1


--Ticket
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where bizu_score >= 850) as "A+",
	avg(li.total_payment) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(li.total_payment) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(li.total_payment) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(li.total_payment) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(li.total_payment) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(li.total_payment) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(li.total_payment) filter (where bizu_score < 300) as "E",
	avg(li.total_payment) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(li.total_payment) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(li.total_payment) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(li.total_payment) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(li.total_payment) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(li.total_payment) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(li.total_payment) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(li.total_payment) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where dp.month_revenue > 400000) as ">400k",
	avg(li.total_payment) filter (where dp.month_revenue > 200000) as ">200k",
	avg(li.total_payment) filter (where dp.month_revenue > 80000) as ">80k",
	avg(li.total_payment) filter (where dp.month_revenue > 40000) as ">40k",
	avg(li.total_payment) filter (where dp.month_revenue > 10000) as ">10k",
	avg(li.total_payment) filter (where dp.month_revenue < 10000) as "<10k",
	avg(li.total_payment) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where dp.age > 20) as ">20",
	avg(li.total_payment) filter (where dp.age > 10) as ">10",
	avg(li.total_payment) filter (where dp.age > 5) as ">5",
	avg(li.total_payment) filter (where dp.age > 3) as ">3",
	avg(li.total_payment) filter (where dp.age > 2) as ">2",
	avg(li.total_payment) filter (where dp.age > 1) as ">1",
	avg(li.total_payment) filter (where dp.age < 1) as "<1",
	avg(li.total_payment) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.total_payment) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(li.total_payment) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(li.total_payment) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(li.total_payment) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(li.total_payment) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(li.total_payment) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(total_payment) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(total_payment) filter (where lead_marketing_channel = 'organico') "organico",
	avg(total_payment) filter (where lead_marketing_channel = 'adwords') "google",
	avg(total_payment) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(total_payment) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(total_payment) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),li.total_payment,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1

--CET
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where bizu_score >= 850) as "A+",
	avg(li.monthly_cet) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(li.monthly_cet) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(li.monthly_cet) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(li.monthly_cet) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(li.monthly_cet) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(li.monthly_cet) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(li.monthly_cet) filter (where bizu_score < 300) as "E",
	avg(li.monthly_cet) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(li.monthly_cet) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(li.monthly_cet) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(li.monthly_cet) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(li.monthly_cet) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(li.monthly_cet) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(li.monthly_cet) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(li.monthly_cet) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where dp.month_revenue > 400000) as ">400k",
	avg(li.monthly_cet) filter (where dp.month_revenue > 200000) as ">200k",
	avg(li.monthly_cet) filter (where dp.month_revenue > 80000) as ">80k",
	avg(li.monthly_cet) filter (where dp.month_revenue > 40000) as ">40k",
	avg(li.monthly_cet) filter (where dp.month_revenue > 10000) as ">10k",
	avg(li.monthly_cet) filter (where dp.month_revenue < 10000) as "<10k",
	avg(li.monthly_cet) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where dp.age > 20) as ">20",
	avg(li.monthly_cet) filter (where dp.age > 10) as ">10",
	avg(li.monthly_cet) filter (where dp.age > 5) as ">5",
	avg(li.monthly_cet) filter (where dp.age > 3) as ">3",
	avg(li.monthly_cet) filter (where dp.age > 2) as ">2",
	avg(li.monthly_cet) filter (where dp.age > 1) as ">1",
	avg(li.monthly_cet) filter (where dp.age < 1) as "<1",
	avg(li.monthly_cet) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(li.monthly_cet) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(li.monthly_cet) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(li.monthly_cet) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(li.monthly_cet) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(li.monthly_cet) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(li.monthly_cet) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(monthly_cet) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(monthly_cet) filter (where lead_marketing_channel = 'organico') "organico",
	avg(monthly_cet) filter (where lead_marketing_channel = 'adwords') "google",
	avg(monthly_cet) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(monthly_cet) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(monthly_cet) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),li.monthly_cet,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1

--Taxa
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where bizu_score >= 850) as "A+",
	avg(o.interest_rate) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(o.interest_rate) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(o.interest_rate) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(o.interest_rate) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(o.interest_rate) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(o.interest_rate) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(o.interest_rate) filter (where bizu_score < 300) as "E",
	avg(o.interest_rate) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(o.interest_rate) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(o.interest_rate) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(o.interest_rate) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(o.interest_rate) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(o.interest_rate) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(o.interest_rate) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(o.interest_rate) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where dp.month_revenue > 400000) as ">400k",
	avg(o.interest_rate) filter (where dp.month_revenue > 200000) as ">200k",
	avg(o.interest_rate) filter (where dp.month_revenue > 80000) as ">80k",
	avg(o.interest_rate) filter (where dp.month_revenue > 40000) as ">40k",
	avg(o.interest_rate) filter (where dp.month_revenue > 10000) as ">10k",
	avg(o.interest_rate) filter (where dp.month_revenue < 10000) as "<10k",
	avg(o.interest_rate) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where dp.age > 20) as ">20",
	avg(o.interest_rate) filter (where dp.age > 10) as ">10",
	avg(o.interest_rate) filter (where dp.age > 5) as ">5",
	avg(o.interest_rate) filter (where dp.age > 3) as ">3",
	avg(o.interest_rate) filter (where dp.age > 2) as ">2",
	avg(o.interest_rate) filter (where dp.age > 1) as ">1",
	avg(o.interest_rate) filter (where dp.age < 1) as "<1",
	avg(o.interest_rate) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(o.interest_rate) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(o.interest_rate) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(o.interest_rate) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(o.interest_rate) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(o.interest_rate) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(o.interest_rate) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(interest_rate) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(interest_rate) filter (where lead_marketing_channel = 'organico') "organico",
	avg(interest_rate) filter (where lead_marketing_channel = 'adwords') "google",
	avg(interest_rate) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(interest_rate) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(interest_rate) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),o.interest_rate,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1

--Prazo
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) as avg_serasa
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1

--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where bizu_score >= 850) as "A+",
	avg(lr.number_of_installments) filter (where bizu_score between 700 and 849.99) as "A-",
	avg(lr.number_of_installments) filter (where bizu_score between 650 and 699.99) as "B+",
	avg(lr.number_of_installments) filter (where bizu_score between 600 and 649.99) as "B-",
	avg(lr.number_of_installments) filter (where bizu_score between 500 and 599.99) as "C+",
	avg(lr.number_of_installments) filter (where bizu_score between 400 and 499.99) as "C-",
	avg(lr.number_of_installments) filter (where bizu_score between 300 and 399.99) as "D",	
	avg(lr.number_of_installments) filter (where bizu_score < 300) as "E",
	avg(lr.number_of_installments) filter (where bizu_score is null) as "Sem bizu"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end >= 750) as "A+",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 600 and 749.99) as "A-",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 450 and 599.99) as "B+",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 350 and 449.99) as "B-",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 250 and 349.99) as "C+",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 150 and 249.99) as "C-",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end between 100 and 149.99) as "D",	
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end < 100) as "E",
	avg(lr.number_of_installments) filter (where case when serasa_coleta is null then o.rating::int else serasa_coleta end is null) as "Sem serasa"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, dp.cnpj, dp.serasa_4,o.rating,max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS')) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date
		from credito_coleta cc
		join direct_prospects dp on left(cc.documento,8) = left(dp.cnpj,8)
		left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
			from direct_prospects dp
			left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
			left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
			left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
			group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
		left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
		where tipo = 'Serasa'
		group by 1,2,3,4) as t1 on left(cnpj,8) = left(cc.documento,8) and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') = max_date) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where dp.state = 'SP') as "SAO PAULO",
	avg(lr.number_of_installments) filter (where dp.state in ('RJ','MG','ES')) as "SUDESTE",
	avg(lr.number_of_installments) filter (where dp.state in ('PR','RS','SC')) as "SUL",
	avg(lr.number_of_installments) filter (where dp.state in ('MA','PI','CE','RN','PE','PB','SE','AL','BA')) as "NORDESTE",
	avg(lr.number_of_installments) filter (where dp.state in ('MT','MS','GO','DF')) as "CENTRO OESTE",
	avg(lr.number_of_installments) filter (where dp.state in ('AM','RR','AP','PA','TO','RO','AC')) as "NORTE",
	avg(lr.number_of_installments) filter (where dp.state is null) as "Sem estado"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where dp.month_revenue > 400000) as ">400k",
	avg(lr.number_of_installments) filter (where dp.month_revenue > 200000) as ">200k",
	avg(lr.number_of_installments) filter (where dp.month_revenue > 80000) as ">80k",
	avg(lr.number_of_installments) filter (where dp.month_revenue > 40000) as ">40k",
	avg(lr.number_of_installments) filter (where dp.month_revenue > 10000) as ">10k",
	avg(lr.number_of_installments) filter (where dp.month_revenue < 10000) as "<10k",
	avg(lr.number_of_installments) filter (where dp.month_revenue is null) as "Sem faturamento"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1



--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where dp.age > 20) as ">20",
	avg(lr.number_of_installments) filter (where dp.age > 10) as ">10",
	avg(lr.number_of_installments) filter (where dp.age > 5) as ">5",
	avg(lr.number_of_installments) filter (where dp.age > 3) as ">3",
	avg(lr.number_of_installments) filter (where dp.age > 2) as ">2",
	avg(lr.number_of_installments) filter (where dp.age > 1) as ">1",
	avg(lr.number_of_installments) filter (where dp.age < 1) as "<1",
	avg(lr.number_of_installments) filter (where dp.age is null) as "Sem idade"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
	avg(lr.number_of_installments) filter (where dp.sector = 'COMERCIO') as "Comércio",
	avg(lr.number_of_installments) filter (where dp.sector = 'SERVICOS') as "Serviços",
	avg(lr.number_of_installments) filter (where dp.sector = 'CONSTRUCAO CIVIL') as "Construção civil",
	avg(lr.number_of_installments) filter (where dp.sector = 'AGROPECUARIA') as "Agropecuaria",
	avg(lr.number_of_installments) filter (where dp.sector = 'INDUSTRIA') as "Industria",
	avg(lr.number_of_installments) filter (where dp.sector = '' or dp.sector is null) as "Sem setor"
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
where lr.status = 'ACCEPTED'
group by 1


--Evolutivo propostas Serasa
select to_date,
	avg(number_of_installments) filter (where lead_marketing_channel = 'facebook') "facebook",
	avg(number_of_installments) filter (where lead_marketing_channel = 'organico') "organico",
	avg(number_of_installments) filter (where lead_marketing_channel = 'adwords') "google",
	avg(number_of_installments) filter (where lead_marketing_channel in ('peixeurbano','fdex','credita')) as "grande parceiros",
	avg(number_of_installments) filter (where lead_marketing_channel in ('madeira','andreozzi','prontocomb','vipac','mobdq','bidu','administradores','vendedoor','jurosbaixos','capitalemp','finanzero','ISF','ifood','vidanova','MRS','payu','americanas','worldsense','IMPERIO','rsassessoria','JEITONOVO','R2A','valmari','CREDSHOPPING','mercado livre','DIGNESS','lio','CREDEXPRESS','andreozziz','VIRGINIA','trustpilot','rentcom')) "pequenos parceiros",
	avg(number_of_installments) filter (where lead_marketing_channel in ('taboola','linkedin','rd','twitter','trustpilot','cn','RD Station','blog','bing','email','experian')) "outros"
from (select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),lr.number_of_installments,case when utm_source is null then 'NULL' when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source = 'RD Station' then 'rd' when utm_source = 'andreozziz' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ú'),'%C3%A7','ç'),'%2B','+'),'%2F','/'),'%C3%A9','é'),'%C3%A3','ã'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-','') end as lead_marketing_channel
	--------------------------
	from direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	join loan_infos li on li.loan_info_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1

--Funil
--------------------------------------------------------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
----------------------------------------------
select to_date(to_char(dp.opt_in_date,'Mon/yy'),'Mon/yy'),
--LEAD
	avg(dp.amount_requested)/1000 as avg_ticket_lead,
--QUALI
	avg(dp.amount_requested) filter (where max_date1 is not null or max_date2 is not null or o.direct_prospect_id is not null)/1000 as avg_ticket_quali,
--OFFER
	avg(o.max_value) filter (where o.direct_prospect_id is not null)/1000 as avg_ticket_offer,
	sum(o.interest_rate * o.max_value) filter (where o.direct_prospect_id is not null)/sum(o.max_value) filter (where o.direct_prospect_id is not null) as avg_interest_rate_offer,
	sum(o.max_number_of_installments * o.max_value) filter (where o.direct_prospect_id is not null)/sum(o.max_value) filter (where o.direct_prospect_id is not null) as avg_term_offer,
--PEDIDO
	avg(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where lr.loan_request_id is not null)/1000 as avg_ticket_request,
	sum(case when lr.taxa_final is null then o.interest_rate else lr.taxa_final end * case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where lr.loan_request_id is not null)/sum(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where lr.loan_request_id is not null) as avg_interest_rate_request,
	sum(case when lr.prazo_solicitado is null then o.max_number_of_installments else lr.prazo_solicitado end * case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where lr.loan_request_id is not null)/sum(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where lr.loan_request_id is not null) as avg_term_request,
--PEDIDO COMPLETO
	avg(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)/1000 as avg_ticket_alldoc,
	sum(case when lr.taxa_final is null then o.interest_rate else lr.taxa_final end * case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)/sum(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) as avg_interest_rate_alldoc,
	sum(case when lr.prazo_solicitado is null then o.max_number_of_installments else lr.prazo_solicitado end * case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null)/sum(case when lr.valor_solicitado is null then o.max_value else lr.valor_solicitado end) filter (where cp.emprestimo_id is not null or lr.status in ('ACCEPTED','REJECTED','Cancelado') or ad.loan_request_id is not null) as avg_term_alldoc,
--LOAN
	avg(lr.value) filter (where lr.status = 'ACCEPTED')/1000 as avg_ticket_loan,
	sum(case when lr.taxa_final is null then o.interest_rate else lr.taxa_final end * lr.value) filter (where lr.status = 'ACCEPTED')/sum(lr.value) filter (where lr.status = 'ACCEPTED') as avg_interest_rate_loan,
	sum(lr.number_of_installments * lr.value) filter (where lr.status = 'ACCEPTED')/sum(lr.value) filter (where lr.status = 'ACCEPTED') as avg_term_loan,
	sum(li.monthly_cet * lr.value) filter (where lr.status = 'ACCEPTED')/sum(lr.value) filter (where lr.status = 'ACCEPTED') as avg_cet_loan
--------------------------
from direct_prospects dp
left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
	from direct_prospects dp
	left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
	left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
	left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
	group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join (select lr.* from loan_requests lr join (select offer_id,max(loan_request_id) filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,min(loan_request_id) as min_id from loan_requests group by 1) as aux on case when aux.max_id is null then min_id else max_id end = lr.loan_request_id) lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
left join loan_infos li on li.loan_info_id = lr.loan_request_id
left join (select distinct emprestimo_id from credito_parecer) as cp on cp.emprestimo_id = lr.loan_request_id
left join alldocs ad on ad.loan_request_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select dp.direct_prospect_id, 
		max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else dp.opt_in_date + interval '10 days' end) as max_date1,
		max(data_coleta + interval '1 millisecond' * id) filter (where (cc.data->>'datetime') = '-1') as max_date2
	from credito_coleta cc
	join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
	left join (select dp.direct_prospect_id, max(accepted_offers.offer_id) acc_offer_id,max(max_request_offers.offer_id) max_offer_id,max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(select o.direct_prospect_id,o.offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id where lr.status = 'ACCEPTED') as accepted_offers on accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(select o.direct_prospect_id, max(o.offer_id) as offer_id from offers o join loan_requests lr on lr.offer_id = o.offer_id group by 1) as max_request_offers on max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (select o.direct_prospect_id,max(o.offer_id) as offer_id from offers o left join loan_requests lr on lr.offer_id = o.offer_id where lr.loan_request_id is null group by 1) as no_request_offer on no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1) as ref_offer on dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join loan_requests lr on lr.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	where tipo in ('Serasa','RelatoBasico','RelatoSocios')
	group by 1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where opt_in_date > '2016-12-31' and opt_in_date < '2018-12-01'
group by 1


--KPIs MEDIOS LOAN FECHAMENTO
select to_date(to_char(lr.loan_date,'Mon/yy'),'Mon/yy') as cohort_loan,case when dp.bizu2_score >= 1100 then 'A+' when dp.bizu2_score >= 850 then 'A-' when dp.bizu2_score >= 720 then 'B+' when dp.bizu2_score >= 600 then 'B-' when dp.bizu2_score >= 500 then 'C+' when dp.bizu2_score >= 400 then 'C-' when dp.bizu2_score >= 300 then 'D' when dp.bizu2_score >= 200 then 'E' when dp.bizu2_score < 200 then 'F' else 'No Bizu2' end as classe_Bizu2,
	sum(li.total_payment) as value_gross,
	sum(lr.number_of_installments * lr.value) as term,
	sum(lr.taxa_final * li.total_payment) as wac,
	sum(li.monthly_cet * li.total_payment) as cet,
	count(*) as units,
	sum(dp.bizu2_score) as bizu2,
	sum(coalesce(serasa_coleta,o.rating,dp.serasa_4)) as serasa,
	sum(dp.lead_score) as lead_score
--------------------------
from direct_prospects dp
join offers o on o.direct_prospect_id = dp.direct_prospect_id
join loan_requests lr on lr.offer_id = o.offer_id
join loan_infos li on li.loan_info_id = lr.loan_request_id
--------------------------TABLE SERASA SCORE
left join (select direct_prospect_id, (data->>'score')::int as serasa_coleta
	from credito_coleta cc
	join(select dp.direct_prospect_id, 
			max(to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id) filter (where to_date(cc.data->'datetime'->>'data','yyyymmdd') <= case when lr.loan_date is not null then lr.loan_date when lr.date_inserted is not null then lr.date_inserted when o.date_inserted is not null then o.date_inserted else lr.loan_date + interval '30 days' end) as max_date1
		from credito_coleta cc
		join direct_prospects dp on dp.direct_prospect_id = cc.origem_id
		join offers o on o.direct_prospect_id = dp.direct_prospect_id
		join loan_requests lr on lr.offer_id = o.offer_id
		where tipo in ('Serasa','RelatoBasico','RelatoSocios') and lr.status = 'ACCEPTED'
		group by 1) as t1 on t1.direct_prospect_id = cc.origem_id and to_timestamp((data->'datetime'->>'data')::text || ' ' || (data->'datetime'->>'hora')::text,'yyyymmdd HH24:MI:SS') + interval '1 millisecond' * id = max_date1) as serasascore on serasascore.direct_prospect_id = dp.direct_prospect_id
where lr.status = 'ACCEPTED'
group by 1,2
order by 1,2





