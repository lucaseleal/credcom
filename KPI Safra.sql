--FPD
select to_date(to_char(loan_date,'Mon/yy'),'Mon/yy'), 
	sum(original_p_and_i) as sum_total,
	(sum(original_p_and_i) filter (where collection_fpd_30 = 1) / sum(original_p_and_i)::float)::decimal(10,6) as "%_sum_fpd_30",
	(sum(original_p_and_i) filter (where collection_fpd_60 = 1) / sum(original_p_and_i)::float)::decimal(10,6) as "%_sum_fpd_60",
	(sum(original_p_and_i) filter (where collection_fpd_90 = 1) / sum(original_p_and_i)::float)::decimal(10,6) as "%_sum_fpd_90",
	(sum(original_p_and_i) filter (where collection_fpd_120 = 1) / sum(original_p_and_i)::float)::decimal(10,6) as "%_sum_fpd_120",
	count(*) as count_total,
	(count(*) filter (where collection_fpd_30 = 1) / count(*)::float)::decimal(10,3) as "%_count_fpd_30",
	(count(*) filter (where collection_fpd_60 = 1) / count(*)::float)::decimal(10,3) as "%_count_fpd_60",
	(count(*) filter (where collection_fpd_90 = 1) / count(*)::float)::decimal(10,3) as "%_count_fpd_90",
	(count(*) filter (where collection_fpd_120 = 1) / count(*)::float)::decimal(10,3) as "%_count_fpd_120"
from(select lr.loan_request_id,lr.loan_date,
	(dpd_soft.max_late_pmt1 > 15)::int as collection_fpd_15,
	(dpd_soft.max_late_pmt1 > 30)::int as collection_fpd_30,
	(dpd_soft.max_late_pmt1 > 60)::int as collection_fpd_60,
	(dpd_soft.max_late_pmt1 > 90)::int as collection_fpd_90,
	(dpd_soft.max_late_pmt1 > 120)::int as collection_fpd_120,
	original_p_and_i
	FROM direct_prospects dp
	join offers o on o.direct_prospect_id = dp.direct_prospect_id
	join loan_requests lr on lr.offer_id = o.offer_id
	--------------------------TABLE PARCELAS
	left join(select lr.loan_request_id,
			max(original_p_and_i) as original_p_and_i
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
			from financeiro_parcela fp 
			join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
			where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
		--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
		left join(select fpp.emprestimo_id, min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
			from financeiro_parcela fp
			join financeiro_planopagamento fpp on fpp.id = fp.plano_id
			where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
			group by 1) as t2 on t2.emprestimo_id = fpp.emprestimo_id
		--TABELA PARA BUSCAR INFORMAÇÕES DO PLANO ORIGINAL
		join(select fpp.emprestimo_id, 
				sum(fp.cobrado) as original_p_and_i, 
				max(fp.vencimento_em) filter (where fp.numero = 1) as original_1st_pmt_due_to, 
				max(fp.vencimento_em) filter (where fp.numero = 2) as original_2nd_pmt_due_to
			from financeiro_planopagamento fpp  
			join (select emprestimo_id, min(id) as plano_id from financeiro_planopagamento group by 1) as fpp_orig on fpp_orig.plano_id = fpp.id
			join financeiro_parcela fp on fp.plano_id = fpp.id
			join (select fpp.emprestimo_id
				from (select emprestimo_id, min(id) id from financeiro_planopagamento group by 1) fpp
				join financeiro_parcela fp on fp.plano_id = fpp.id
				where fp.numero = 1 and fp.vencimento_em <= current_date) as t1 on t1.emprestimo_id = fpp.emprestimo_id
			group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
		where fp.status not in ('Cancelada','EmEspera')
		group by 1) as parcelas on parcelas.loan_request_id = lr.loan_request_id
	--------------------------TABLE DPD SOFT
	left join(select lr.loan_request_id,
			max((case when fp.numero_ajustado <> 1 then null else (case when ParcSubst = 1 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt1_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt1_venc else null end) else (case when fp.pago_em is null then current_date - pmt1_venc else fp.pago_em - pmt1_venc end) end) end) end)) as max_late_pmt1,
			max((case when fp.numero_ajustado <> 2 then null else (case when ParcSubst = 2 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt2_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt2_venc else null end) else (case when fp.pago_em is null then current_date - pmt2_venc else fp.pago_em - pmt2_venc end) end) end) end)) as max_late_pmt2,
			max((case when fp.numero_ajustado <> 3 then null else (case when ParcSubst = 3 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt3_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt3_venc else null end) else (case when fp.pago_em is null then current_date - pmt3_venc else fp.pago_em - pmt3_venc end) end) end) end)) as max_late_pmt3,
			max((case when fp.numero_ajustado <> 4 then null else (case when ParcSubst = 4 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt4_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt4_venc else null end) else (case when fp.pago_em is null then current_date - pmt4_venc else fp.pago_em - pmt4_venc end) end) end) end)) as max_late_pmt4,
			max((case when fp.numero_ajustado <> 5 then null else (case when ParcSubst = 5 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt5_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt5_venc else null end) else (case when fp.pago_em is null then current_date - pmt5_venc else fp.pago_em - pmt5_venc end) end) end) end)) as max_late_pmt5,
			max((case when fp.numero_ajustado <> 6 then null else (case when ParcSubst = 6 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt6_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt6_venc else null end) else (case when fp.pago_em is null then current_date - pmt6_venc else fp.pago_em - pmt6_venc end) end) end) end)) as max_late_pmt6,
			max((case when fp.numero_ajustado <> 7 then null else (case when ParcSubst = 7 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt7_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt7_venc else null end) else (case when fp.pago_em is null then current_date - pmt7_venc else fp.pago_em - pmt7_venc end) end) end) end)) as max_late_pmt7,
			max((case when fp.numero_ajustado <> 8 then null else (case when ParcSubst = 8 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt8_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt8_venc else null end) else (case when fp.pago_em is null then current_date - pmt8_venc else fp.pago_em - pmt8_venc end) end) end) end)) as max_late_pmt8,
			max((case when fp.numero_ajustado <> 9 then null else (case when ParcSubst = 9 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt9_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt9_venc else null end) else (case when fp.pago_em is null then current_date - pmt9_venc else fp.pago_em - pmt9_venc end) end) end) end)) as max_late_pmt9,
			max((case when fp.numero_ajustado <> 10 then null else (case when ParcSubst = 10 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt10_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt10_venc else null end) else (case when fp.pago_em is null then current_date - pmt10_venc else fp.pago_em - pmt10_venc end) end) end) end)) as max_late_pmt10,
			max((case when fp.numero_ajustado <> 11 then null else (case when ParcSubst = 11 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt11_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt11_venc else null end) else (case when fp.pago_em is null then current_date - pmt11_venc else fp.pago_em - pmt11_venc end) end) end) end)) as max_late_pmt11,
			max((case when fp.numero_ajustado <> 12 then null else (case when ParcSubst = 12 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt12_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt12_venc else null end) else (case when fp.pago_em is null then current_date - pmt12_venc else fp.pago_em - pmt12_venc end) end) end) end)) as max_late_pmt12,
			max((case when fp.numero_ajustado <> 13 then null else (case when ParcSubst = 13 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt13_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt13_venc else null end) else (case when fp.pago_em is null then current_date - pmt13_venc else fp.pago_em - pmt13_venc end) end) end) end)) as max_late_pmt13,
			max((case when fp.numero_ajustado <> 14 then null else (case when ParcSubst = 14 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt14_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt14_venc else null end) else (case when fp.pago_em is null then current_date - pmt14_venc else fp.pago_em - pmt14_venc end) end) end) end)) as max_late_pmt14,
			max((case when fp.numero_ajustado <> 15 then null else (case when ParcSubst = 15 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt15_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt15_venc else null end) else (case when fp.pago_em is null then current_date - pmt15_venc else fp.pago_em - pmt15_venc end) end) end) end)) as max_late_pmt15,
			max((case when fp.numero_ajustado <> 16 then null else (case when ParcSubst = 16 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt16_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt16_venc else null end) else (case when fp.pago_em is null then current_date - pmt16_venc else fp.pago_em - pmt16_venc end) end) end) end)) as max_late_pmt16,
			max((case when fp.numero_ajustado <> 17 then null else (case when ParcSubst = 17 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt17_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt17_venc else null end) else (case when fp.pago_em is null then current_date - pmt17_venc else fp.pago_em - pmt17_venc end) end) end) end)) as max_late_pmt17,
			max((case when fp.numero_ajustado <> 18 then null else (case when ParcSubst = 18 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt18_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt18_venc else null end) else (case when fp.pago_em is null then current_date - pmt18_venc else fp.pago_em - pmt18_venc end) end) end) end)) as max_late_pmt18,
			max((case when fp.numero_ajustado <> 19 then null else (case when ParcSubst = 19 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt19_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt19_venc else null end) else (case when fp.pago_em is null then current_date - pmt19_venc else fp.pago_em - pmt19_venc end) end) end) end)) as max_late_pmt19,
			max((case when fp.numero_ajustado <> 20 then null else (case when ParcSubst = 20 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt20_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt20_venc else null end) else (case when fp.pago_em is null then current_date - pmt20_venc else fp.pago_em - pmt20_venc end) end) end) end)) as max_late_pmt20,
			max((case when fp.numero_ajustado <> 21 then null else (case when ParcSubst = 21 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt21_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt21_venc else null end) else (case when fp.pago_em is null then current_date - pmt21_venc else fp.pago_em - pmt21_venc end) end) end) end)) as max_late_pmt21,
			max((case when fp.numero_ajustado <> 22 then null else (case when ParcSubst = 22 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt22_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt22_venc else null end) else (case when fp.pago_em is null then current_date - pmt22_venc else fp.pago_em - pmt22_venc end) end) end) end)) as max_late_pmt22,
			max((case when fp.numero_ajustado <> 23 then null else (case when ParcSubst = 23 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt23_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt23_venc else null end) else (case when fp.pago_em is null then current_date - pmt23_venc else fp.pago_em - pmt23_venc end) end) end) end)) as max_late_pmt23,
			max((case when fp.numero_ajustado <> 24 then null else (case when ParcSubst = 24 then (case when fp.pago_em is null then current_date - vencparcsubst else fp.pago_em - VencParcSubst end) else (case when current_date < pmt24_venc then (case when fp.pago_em < current_date then fp.pago_em - pmt24_venc else null end) else (case when fp.pago_em is null then current_date - pmt24_venc else fp.pago_em - pmt24_venc end) end) end) end)) as max_late_pmt24
		from loan_requests lr
		join financeiro_planopagamento fpp on fpp.emprestimo_id = lr.loan_request_id
		--TABELA PARA BUSCAR A DATA DE PAGAMENTO DAS PARCELAS NA ORDEM EM QUE ELAS OCORRERAM
		join (select row_number() over (partition by fpp.emprestimo_id order by fp.pago_em,fp.vencimento_em) as numero_ajustado,fp.* 
			from financeiro_parcela fp 
			join financeiro_planopagamento fpp on fpp.id = fp.plano_id 
			where fp.status not in ('Cancelada','EmEspera')) fp on fp.plano_id = fpp.id
		--TABELA PARA BUSCAR A PARCELA E O VENCIMENTO DO PLANO QUE FOI SUBSTITUIDO
		left join(select fpp.emprestimo_id, fp.plano_id,min(vencimento_em) as VencParcSubst, min(numero) as ParcSubst
			from financeiro_parcela fp
			join financeiro_planopagamento fpp on fpp.id = fp.plano_id
			where fp.status = 'Cancelada' and fpp.status not in ('Cancelado','EmEspera')
			group by 1,2) as t2 on t2.emprestimo_id = fpp.emprestimo_id
		--TABELA PARA BUSCAR A DATA DE VENCIMENTO DAS PARCELAS NA ORDEM EM QUE ELAS FORAM CONTRATADAS
		join(select fpp.emprestimo_id,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fpp.id = min_plano.min_id) as pmt2_orig_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 1 and fpp.id = min_plano.min_id) as pmt1_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 2 and fp.status not in ('Cancelada','EmEspera')) as pmt2_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 3 and fp.status not in ('Cancelada','EmEspera')) as pmt3_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 4 and fp.status not in ('Cancelada','EmEspera')) as pmt4_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 5 and fp.status not in ('Cancelada','EmEspera')) as pmt5_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 6 and fp.status not in ('Cancelada','EmEspera')) as pmt6_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 7 and fp.status not in ('Cancelada','EmEspera')) as pmt7_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 8 and fp.status not in ('Cancelada','EmEspera')) as pmt8_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 9 and fp.status not in ('Cancelada','EmEspera')) as pmt9_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 10 and fp.status not in ('Cancelada','EmEspera')) as pmt10_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 11 and fp.status not in ('Cancelada','EmEspera')) as pmt11_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 12 and fp.status not in ('Cancelada','EmEspera')) as pmt12_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 13 and fp.status not in ('Cancelada','EmEspera')) as pmt13_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 14 and fp.status not in ('Cancelada','EmEspera')) as pmt14_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 15 and fp.status not in ('Cancelada','EmEspera')) as pmt15_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 16 and fp.status not in ('Cancelada','EmEspera')) as pmt16_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 17 and fp.status not in ('Cancelada','EmEspera')) as pmt17_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 18 and fp.status not in ('Cancelada','EmEspera')) as pmt18_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 19 and fp.status not in ('Cancelada','EmEspera')) as pmt19_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 20 and fp.status not in ('Cancelada','EmEspera')) as pmt20_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 21 and fp.status not in ('Cancelada','EmEspera')) as pmt21_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 22 and fp.status not in ('Cancelada','EmEspera')) as pmt22_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 23 and fp.status not in ('Cancelada','EmEspera')) as pmt23_venc,
				min(fp.vencimento_em) filter (where fp.numero_ajustado = 24 and fp.status not in ('Cancelada','EmEspera')) as pmt24_venc,
				sum(fp.cobrado) filter (where fpp.id = min_plano.min_id) as original_p_and_i
			from financeiro_planopagamento fpp
			join (select row_number() over (partition by fpp.emprestimo_id order by fp.vencimento_em) as numero_ajustado,fp.* from financeiro_parcela fp join financeiro_planopagamento fpp on fpp.id = fp.plano_id where fp.status not in ('Cancelada','EmEspera')) as fp on fp.plano_id = fpp.id
			join (select emprestimo_id,min(id) as min_id from financeiro_planopagamento group by 1) as min_plano on min_plano.emprestimo_id = fpp.emprestimo_id
			group by 1) vencimentos on vencimentos.emprestimo_id = lr.loan_request_id
		where fp.status not in ('Cancelada','EmEspera')
		group by 1) as DPD_Soft on DPD_Soft.loan_request_id = lr.loan_request_id
	where lr.status = 'ACCEPTED') as t1
group by 1
order by 1
